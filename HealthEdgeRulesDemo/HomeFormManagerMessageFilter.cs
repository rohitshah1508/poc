/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
﻿using System;
using System.Windows.Forms;
using System.Linq;

namespace HealthEdge.Manager
{
    public class HomeFormManagerMessageFilter : IMessageFilter
    {
        private const int WM_KEYDOWN = 0x100;
        private const string HOME_FORM_NAME = "HomeForm";

        /// <summary>
        /// When the Ctrl+Tab key combination is used, add captions to the documents so the user can 
        /// easily activate a card
        /// </summary>
        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == WM_KEYDOWN)
            {
                Keys keyCode = (Keys)(int)m.WParam & Keys.KeyCode;
                if (Control.ModifierKeys == Keys.Control && keyCode == (Keys.Tab))
                {
                    var foundForm = Application.OpenForms.Cast<Form>().FirstOrDefault(openForm => openForm.GetType() == typeof(HomeForm));
                    if (foundForm != null)
                    {
                        (foundForm as HomeForm).ChangeNames(true);
                    }
                }
            }

            return false;
        }
    }
}
