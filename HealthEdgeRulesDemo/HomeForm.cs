/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;

using HealthEdge.Controls;
using HealthEdge.Framework;
using HealthEdge.SecurityAndAdministration;
using HealthEdge.Utility;

namespace HealthEdgeRulesDemo
{
    public partial class HomeForm : HomeFormBase
    {
        #region Enums

        private enum ManagerWidgetTypes
        {
            Claims = 0, 
            Members = 1
        }

        #endregion

        #region Private Fields

		private static bool _loggedIn;

        private ApplicationConfiguration _applicationConfiguration;

        private JSONPayloadDataResponse jsonPayloadDataResponse;

        #endregion

        #region Statics

        public static bool LoggedIn
        {
            get
            {
                return _loggedIn;
            }
        }
        
        #endregion

        #region Constructor/Destructor

        /// <summary>
		/// Home form class for HealthRules Manager
		/// </summary>
		public HomeForm()
		{            
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            InitializeComponent();

            panelLogout.Parent = pictureBox;
            panelLogout.Dock = DockStyle.Right;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        #endregion
       
        #region Public Methods

        public void ChangeNames(bool setNames)
        {
            //homeTab1.ChangeNames(setNames);
        }                

        public void createItemTaskRowHandler()
        {
            CreateItem();
        }

        public void CreateItem()
        {
            
        }        
        
        private void homeTab1_Load(object sender, EventArgs e)
        {            
            LoadHomeTab1();
        }

        private void LoadHomeTab1()
        {
            string userName = string.Empty;
            _applicationConfiguration = GetConfigurationUsingSection();
            using (LoginForm login = new LoginForm(_applicationConfiguration))
            {
                bool loginRequired = _applicationConfiguration.ShowLoginAlways ? true : login.IsLoginRequired();
                if (loginRequired)
                {
                    login.ShowDialog(this);

                    if (login.DialogResult == DialogResult.Cancel)
                    {
                        this.Close();
                        Application.Exit();
                        return;
                    }                                        
                }
                jsonPayloadDataResponse = login.JSONPayloadDataResponse;
            }

            SetupHomeTab();

            this.Text = $"{this.Text} - ({jsonPayloadDataResponse?.UserName})";
            this.toolStripStatusLabelServer.Text = $"Server: {_applicationConfiguration.ForgeRockHost}/{_applicationConfiguration.Realm}";
            this.toolTip.SetToolTip(buttonLogout, "Click here to logout");
        }
        
        private void SetupHomeTab()
		{
            _loggedIn = true;
            this.homeTab1.ShowControl(_applicationConfiguration.RoleMapping, jsonPayloadDataResponse?.Roles);
            
            InitImages();            
        }

        private void InitImages()
		{
            string path = $"{AssemblyDirectory}{Path.DirectorySeparatorChar}HealthEdge.Manager.resources.dll";
            Assembly asm = Assembly.LoadFrom(path);

            ResourceManager rm = new System.Resources.ResourceManager("HealthEdge.Manager", asm);

            if (rm == null)
                return;

            this.Icon = ((Icon)(rm.GetObject("Logo")));

            this.pictureBox.Image = ((Image)(rm.GetObject("HealthEdgeLogo")));

        }              

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            SecurityAdminWrapper.Logout($"{_applicationConfiguration.ForgeRockHost}/{_applicationConfiguration.Realm}/token/revoke", _applicationConfiguration.Client); 
            this.Close();
            Application.Exit();
            string path = $"{AssemblyDirectory}{Path.DirectorySeparatorChar}HealthEdgeRulesDemo.exe";
            Process.Start(path);
        }

        private ApplicationConfiguration GetConfigurationUsingSection()
        {
            var applicationSettings = ConfigurationManager.GetSection("ApplicationSettings") as NameValueCollection;
            ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration()
            {
                ForgeRockHost = applicationSettings["ForgeRockHost"],
                Realm = applicationSettings["Realm"],
                Client = applicationSettings["Client"],
                RoleMapping = applicationSettings["RoleMapping"].Split(new char[] { '|' }),
                ShowLoginAlways = Convert.ToBoolean(applicationSettings["ShowLoginAlways"])
            };

            return applicationConfiguration;
        }

        #endregion
    }

    
}
