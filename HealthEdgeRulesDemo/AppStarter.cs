/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.Windows.Forms;
using System.Xml.Serialization;

using DevExpress.XtraEditors;

using HealthEdge.Utility;


namespace HealthEdge.Manager
{
    /// <summary>
    /// The main entry point for HealthRules Manager
    /// </summary>
    public static class AppStarter
	{
        [STAThread]
		static void Main(string[] args)
		{
            // Prevent running client apps if the correct .NET version is not installed
            if (!VersionUtil.CheckAssemblyVersion())
                return;

            WindowsFormsSettings.LoadApplicationSettings();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //WindowsFormsSettings.ForceDirectXPaint();
            XtraMessageBox.SmartTextWrap = true;

            //WindowsFormsSettings.AllowAutoScale = DevExpress.Utils.DefaultBoolean.True;
            //WindowsFormsSettings.AllowDpiScale = true;
            //WindowsFormsSettings.SetDPIAware();

            if (ConfigSettings.Instance.HEConfigSettings != null)
            {
                if (ConfigSettings.Instance.HEConfigSettings.UseSkins)
                {
                    DevExpress.UserSkins.BonusSkins.Register();
                    DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(DevExpress.UserSkins.HealthEdge2015).Assembly);
                    DevExpress.Skins.SkinManager.EnableFormSkins();

                    WindowsFormsSettings.FormThickBorder = true;
                    WindowsFormsSettings.ThickBorderWidth = 2;
                    WindowsFormsSettings.BackgroundSkinningMode = BackgroundSkinningMode.AllColors;
                    WindowsFormsSettings.SvgImageRenderingMode = DevExpress.Utils.Svg.SvgImageRenderingMode.HighSpeed;

                    // Citrix and Remote Desktop don't support rendering transparent windows so disable form shadows at the application level
                    WindowsFormsSettings.DisableWindowShadows();

                    // LORI TBD - For DirectX testing
                    //WindowsFormsSettings.ForcePaintApiDiagnostics(DevExpress.Utils.Diagnostics.PaintApiDiagnosticsLevel.Throw);
                }

                if (args.Length > 0)
                {
                    TestUtil.IsUnitTestMode = bool.Parse(args[0]);
                }

                Application.Run(new HomeForm());
            }
		}
	}
}
