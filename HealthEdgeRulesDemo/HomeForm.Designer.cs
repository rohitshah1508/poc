/******************************************************************************
 *
 * Copyright Â© 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
namespace HealthEdgeRulesDemo
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeForm));
            this.panelControlDetail = new System.Windows.Forms.Panel();
            this.panelLogout = new System.Windows.Forms.Panel();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelServer = new System.Windows.Forms.ToolStripStatusLabel();
            this.homeTab1 = new HealthEdgeRulesDemo.Controls.HomeTabDemo();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemView = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panelControlDetail.SuspendLayout();
            this.panelLogout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControlDetail
            // 
            this.panelControlDetail.BackColor = System.Drawing.Color.Transparent;
            this.panelControlDetail.Controls.Add(this.panelLogout);
            this.panelControlDetail.Controls.Add(this.pictureBox);
            this.panelControlDetail.Controls.Add(this.statusStrip);
            this.panelControlDetail.Controls.Add(this.homeTab1);
            this.panelControlDetail.Controls.Add(this.menuStrip);
            this.panelControlDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlDetail.Location = new System.Drawing.Point(0, 0);
            this.panelControlDetail.Name = "panelControlDetail";
            this.panelControlDetail.Padding = new System.Windows.Forms.Padding(8);
            this.panelControlDetail.Size = new System.Drawing.Size(734, 361);
            this.panelControlDetail.TabIndex = 3;
            // 
            // panelLogout
            // 
            this.panelLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelLogout.Controls.Add(this.buttonLogout);
            this.panelLogout.Location = new System.Drawing.Point(683, 33);
            this.panelLogout.Name = "panelLogout";
            this.panelLogout.Size = new System.Drawing.Size(42, 34);
            this.panelLogout.TabIndex = 6;
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonLogout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonLogout.FlatAppearance.BorderSize = 2;
            this.buttonLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogout.Image = global::HealthEdgeRulesDemo.Properties.Resources.Logout;
            this.buttonLogout.Location = new System.Drawing.Point(3, 1);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(32, 32);
            this.buttonLogout.TabIndex = 5;
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(154)))), ((int)(((byte)(243)))));
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox.ImageLocation = "";
            this.pictureBox.InitialImage = null;
            this.pictureBox.Location = new System.Drawing.Point(8, 32);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(718, 36);
            this.pictureBox.TabIndex = 4;
            this.pictureBox.TabStop = false;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelServer});
            this.statusStrip.Location = new System.Drawing.Point(8, 331);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(718, 22);
            this.statusStrip.TabIndex = 3;
            // 
            // toolStripStatusLabelServer
            // 
            this.toolStripStatusLabelServer.Name = "toolStripStatusLabelServer";
            this.toolStripStatusLabelServer.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabelServer.Text = "Server:";
            // 
            // homeTab1
            // 
            this.homeTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.homeTab1.BackColor = System.Drawing.Color.Transparent;
            this.homeTab1.Location = new System.Drawing.Point(8, 71);
            this.homeTab1.Name = "homeTab1";
            this.homeTab1.Size = new System.Drawing.Size(715, 257);
            this.homeTab1.TabIndex = 1;
            this.homeTab1.Load += new System.EventHandler(this.homeTab1_Load);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.menuStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip.Font = new System.Drawing.Font("Tahoma", 9F);
            this.menuStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFile,
            this.toolStripMenuItemView,
            this.toolStripMenuItemHelp});
            this.menuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip.Location = new System.Drawing.Point(8, 8);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(0);
            this.menuStrip.Size = new System.Drawing.Size(718, 24);
            this.menuStrip.TabIndex = 2;
            // 
            // toolStripMenuItemFile
            // 
            this.toolStripMenuItemFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.toolStripMenuItemFile.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemFile.Name = "toolStripMenuItemFile";
            this.toolStripMenuItemFile.Size = new System.Drawing.Size(36, 24);
            this.toolStripMenuItemFile.Text = "&File";
            // 
            // toolStripMenuItemView
            // 
            this.toolStripMenuItemView.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemView.Name = "toolStripMenuItemView";
            this.toolStripMenuItemView.Size = new System.Drawing.Size(46, 24);
            this.toolStripMenuItemView.Text = "&View";
            // 
            // toolStripMenuItemHelp
            // 
            this.toolStripMenuItemHelp.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemHelp.Name = "toolStripMenuItemHelp";
            this.toolStripMenuItemHelp.Size = new System.Drawing.Size(43, 24);
            this.toolStripMenuItemHelp.Text = "&Help";
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(734, 361);
            this.Controls.Add(this.panelControlDetail);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "HomeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HealthRules Manager";
            this.panelControlDetail.ResumeLayout(false);
            this.panelControlDetail.PerformLayout();
            this.panelLogout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
		private HealthEdgeRulesDemo.Controls.HomeTabDemo homeTab1;
        private System.Windows.Forms.Panel panelControlDetail;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelServer;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemView;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemHelp;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Panel panelLogout;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

