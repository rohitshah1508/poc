/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

using HealthEdge.Framework;
using HealthEdgeRulesDemo.Controls.Members;
using HealthEdgeRulesDemo.Controls.Claims;

namespace HealthEdgeRulesDemo.Controls
{
    public partial class HomeTab : UserControl
    {
        #region Static Fields

        public static readonly int xPos = 15;
        public static readonly int yStartPos = 55;
        public static readonly int yPos = 40;
        public static readonly int yDiff = 25;

        public static readonly string ClaimCardLabel = "claims";
        public static readonly string MemberCardLabel = "members";
        public static readonly string FinancialCardLabel = "financials";
        public static readonly string ProviderCardLabel = "provider";
        public static readonly string OtherCardLabel = "others";
        public static readonly string FundingCardLabel = "funding";
        public static readonly string UtilizationCardLabel = "utilization";
        public static readonly string AdministrationCardLabel = "administration";
        public static readonly string FavoritesCardLabel = "favorites";
        public static readonly string WorkbasketCardLabel = "my work";

        public static readonly string CLAIM_CARD_NAME = "documentClaims";
        public static readonly string MEMBER_CARD_NAME = "documentMembers";
        public static readonly string FINANCIAL_CARD_NAME = "documentFinancials";
        public static readonly string PROVIDER_CARD_NAME = "documentProviders";
        public static readonly string OTHER_CARD_NAME = "documentOthers";
        public static readonly string FUNDING_CARD_NAME = "documentFunding";
        public static readonly string UTILIZATION_CARD_NAME = "documentUtilization";
        public static readonly string ADMINISTRATION_CARD_NAME = "documentAdministration";
        public static readonly string FAVORITES_CARD_NAME = "documentFavorites";
        public static readonly string WORKBASKET_CARD_NAME = "documentWorkbasket";

        #endregion

        #region Private Fields

        private bool _skipMessage = false;
        private bool _displayedMessage = false;
        private bool _useDefaultLayout = false;
        private bool _isEditFromWorkbasket = false;

        private MemberWidgetControl _memberControl;
        private ClaimWidgetControl _claimControl;        
        private List<IWidgetControl> _widgetControls = new List<IWidgetControl>();

        private Hashtable _documentLookup = new Hashtable();
        private Hashtable _removedDocuments = new Hashtable();
        private Hashtable _currentDocuments = new Hashtable();
        private List<Control> _overlappingControls = new List<Control>();
        private List<string> _allCells = new List<string>();

        #endregion

        #region Constructor/Destructor

        public HomeTab()
        {
            InitializeComponent();

            _documentLookup.Add(HomeTab.CLAIM_CARD_NAME, HomeTab.ClaimCardLabel);
            _documentLookup.Add(HomeTab.MEMBER_CARD_NAME, HomeTab.MemberCardLabel);
            _documentLookup.Add(HomeTab.FINANCIAL_CARD_NAME, HomeTab.FinancialCardLabel);
            _documentLookup.Add(HomeTab.PROVIDER_CARD_NAME, HomeTab.ProviderCardLabel);
            _documentLookup.Add(HomeTab.OTHER_CARD_NAME, HomeTab.OtherCardLabel);
            _documentLookup.Add(HomeTab.FUNDING_CARD_NAME, HomeTab.FundingCardLabel);
            _documentLookup.Add(HomeTab.UTILIZATION_CARD_NAME, HomeTab.UtilizationCardLabel);
            _documentLookup.Add(HomeTab.ADMINISTRATION_CARD_NAME, HomeTab.AdministrationCardLabel);
            _documentLookup.Add(HomeTab.FAVORITES_CARD_NAME, HomeTab.FavoritesCardLabel);
            _documentLookup.Add(HomeTab.WORKBASKET_CARD_NAME, HomeTab.WorkbasketCardLabel);

            widgetView.QueryControl += widgetView_QueryControl;
            widgetView.PopupMenuShowing += widgetView_PopupMenuShowing;
            widgetView.ControlReleasing += widgetView_ControlReleasing;
            widgetView.DocumentAdded += widgetView_DocumentAdded;
            widgetView.DocumentClosed += widgetView_DocumentClosed;
            widgetView.DocumentSelectorHidden += widgetView_DocumentSelectorHidden;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                SaveLayout();

                if (_documentLookup != null)
                    _documentLookup.Clear();

                if (_removedDocuments != null)
                    _removedDocuments.Clear();

                if (_currentDocuments != null)
                    _currentDocuments.Clear();

                if (_overlappingControls != null)
                    _overlappingControls.Clear();

                if (_memberControl != null && !_memberControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_memberControl))
                        _widgetControls.Remove(_memberControl);
                    _memberControl.Dispose();
                    _memberControl = null;
                }

                if (_providerControl != null && !_providerControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_providerControl))
                        _widgetControls.Remove(_providerControl);
                    _providerControl.Dispose();
                    _providerControl = null;
                }

                if (_claimControl != null && !_claimControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_claimControl))
                        _widgetControls.Remove(_claimControl);
                    _claimControl.Dispose();
                    _claimControl = null;
                }

                if (_financialControl != null && !_financialControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_financialControl))
                        _widgetControls.Remove(_financialControl);
                    _financialControl.Dispose();
                    _financialControl = null;
                }

                if (_fundingControl != null && !_fundingControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_fundingControl))
                        _widgetControls.Remove(_fundingControl);
                    _fundingControl.Dispose();
                    _fundingControl = null;
                }

                if (_utilizationControl != null && !_utilizationControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_utilizationControl))
                        _widgetControls.Remove(_utilizationControl);
                    _utilizationControl.Dispose();
                    _utilizationControl = null;
                }

                if (_otherControl != null && !_otherControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_otherControl))
                        _widgetControls.Remove(_otherControl);
                    _otherControl.Dispose();
                    _otherControl = null;
                }

                if (_administrationControl != null && !_administrationControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_administrationControl))
                        _widgetControls.Remove(_administrationControl);
                    _administrationControl.Dispose();
                    _administrationControl = null;
                }

                if (_favoritesControl != null && !_favoritesControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_favoritesControl))
                        _widgetControls.Remove(_favoritesControl);
                    _favoritesControl.Dispose();
                    _favoritesControl = null;
                }

                if (documentWorkbasket != null && !documentWorkbasket.IsDisposing && documentWorkbasket.CustomHeaderButtons?.Count > 0)
                {
                    documentWorkbasket.CustomButtonClick -= documentWorkbasket_CustomButtonClick;
                    if (_refreshButton != null && !_refreshButton.IsDisposing)
                        _refreshButton.Dispose();
                    _refreshButton = null;
                    documentWorkbasket.CustomHeaderButtons.Clear();
                }

                if (_workbasketControl != null && !_workbasketControl.IsDisposed)
                {
                    if (_widgetControls != null && _widgetControls.Contains(_workbasketControl))
                        _widgetControls.Remove(_workbasketControl);
                    _workbasketControl.Dispose();
                    _workbasketControl = null;
                }

                if (_widgetControls != null)
                    _widgetControls.Clear();

                if (widgetView != null && !widgetView.IsDisposing)
                {
                    widgetView.QueryControl -= widgetView_QueryControl;
                    widgetView.PopupMenuShowing -= widgetView_PopupMenuShowing;
                    widgetView.ControlReleasing -= widgetView_ControlReleasing;
                    widgetView.DocumentAdded -= widgetView_DocumentAdded;
                    widgetView.DocumentClosed -= widgetView_DocumentClosed;
                    widgetView.DocumentSelectorHidden -= widgetView_DocumentSelectorHidden;

                    if (widgetView.Documents != null)
                        widgetView.Documents.Clear();
                    widgetView = null;
                }

                if (components != null)
                    components.Dispose();
            }

            base.Dispose(disposing);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Set the document caption when using Ctrl+Tab key combination
        /// </summary>
        public void ChangeNames(bool setNames)
        {
            string documentLabel = "document";
            int len = documentLabel.Length;
            foreach (Document document in widgetView.Documents)
            {
                if (setNames)
                {
                    document.Caption = document.ControlName.Substring(len).ToLower();

                    // Change the label for the workbasket card to 'my work' for consistency
                    if (string.Compare(document.Caption, "workbasket") == 0)
                        document.Caption = "my work";
                }
                else
                    document.Caption = string.Empty;
            }
        }

        public void SuspendCardLayout()
        {
            widgetView.BeginUpdate();
        }

        public void ResumeCardLayout()
        {
            widgetView.EndUpdate();
        }

        public void ExpandWidgets()
        {
            widgetView.BeginUpdate();

            for (int i = 0; i < _widgetControls.Count; i++)
            {
                IWidgetControl control = _widgetControls[i];
                control.SetShowLinksAtStartup();
                control.UpdateLinks();
            }

            widgetView.EndUpdate();
        }

        /// <summary>
        /// Update the widget control's height when showing/hiding quick links
        /// </summary>
        public void SetWidgetHeight(IWidgetControl widgetControl, int height)
        {
            Document document = widgetView.Manager.GetDocument(widgetControl.WidgetControl) as Document;
            document.Height = height;
            widgetView.LayoutChanged();
        }

        /// <summary>
        /// Hide all quick links widgets except on the input widget control
        /// </summary>
        public void UpdateWidget(IWidgetControl widgetControl, int height)
        {
            widgetView.BeginUpdate();

            Document document = widgetView.Manager.GetDocument(widgetControl.WidgetControl) as Document;
            document.Height = height;

            for (int i = 0; i < _widgetControls.Count; i++)
            {
                IWidgetControl control = _widgetControls[i];
                if (control == widgetControl)
                    continue;

                control.HideLinks();
            }

            widgetView.EndUpdate();
        }

        /// <summary>
        /// Show or hide the input card based on the input parameters
        /// </summary>
        public void ShowHideDocument(string caption, bool showCard)
        {
            if (string.Compare(caption, WorkbasketCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentWorkbasket);
            }

            if (string.Compare(caption, ClaimCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentClaims);
            }

            if (string.Compare(caption, MemberCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentMembers);
            }

            if (string.Compare(caption, FinancialCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentFinancials);
            }

            if (string.Compare(caption, ProviderCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentProviders);
            }

            if (string.Compare(caption, FundingCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentFunding);
            }

            if (string.Compare(caption, OtherCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentOthers);
            }

            if (string.Compare(caption, UtilizationCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentUtilization);
            }

            if (string.Compare(caption, AdministrationCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentAdministration);
            }

            if (string.Compare(caption, FavoritesCardLabel) == 0)
            {
                if (showCard)
                    ShowCard(caption);
                else
                    HideCard(caption, documentFavorites);
            }

            if (_displayedMessage && _useDefaultLayout)
                ResetDefaultCardLayout();

            if (_displayedMessage)
                _skipMessage = true;
        }

        /// <summary>
        /// Remove cards based on permissions
        /// </summary>
        public void RemoveDocumentsWithoutPermissions()
        {
            if (!WorkbasketWidgetControl.HasPermissions())
                RemoveDocument(WorkbasketCardLabel, documentWorkbasket);

            if (!MemberWidgetControl.HasPermissions())
                RemoveDocument(MemberCardLabel, documentMembers);

            if (!ProviderWidgetControl.HasPermissions())
                RemoveDocument(ProviderCardLabel, documentProviders);

            if (!ClaimWidgetControl.HasPermissions())
                RemoveDocument(ClaimCardLabel, documentClaims);

            if (!FinancialWidgetControl.HasPermissions())
                RemoveDocument(FinancialCardLabel, documentFinancials);

            if (!FundingWidgetControl.HasPermissions())
                RemoveDocument(FundingCardLabel, documentFunding);

            if (!UtilizationWidgetControl.HasPermissions())
                RemoveDocument(UtilizationCardLabel, documentUtilization);

            if (!OtherWidgetControl.HasPermissions())
                RemoveDocument(OtherCardLabel, documentOthers);

            if (!AdministrationWidgetControl.HasPermissions())
                RemoveDocument(AdministrationCardLabel, documentAdministration);

            if (!FavoritesWidgetControl.HasPermissions())
                RemoveDocument(FavoritesCardLabel, documentFavorites);
        }

        /// <summary>
        /// Indicates if the user has permission to access member card links
        /// </summary>
        public bool HasMemberPermissions() => MemberWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access provider card links
        /// </summary>
        public bool HasProviderPermissions() => ProviderWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access claim card links
        /// </summary>
        public bool HasClaimPermissions() => ClaimWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access financial card links
        /// </summary>
        public bool HasFinancialPermissions() => FinancialWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access funding card links
        /// </summary>
        public bool HasFundingPermissions() => FundingWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access utilization card links
        /// </summary>
        public bool HasUtilizationPermissions() => UtilizationWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access other card links
        /// </summary>
        public bool HasOtherPermissions() => OtherWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access administration card links
        /// </summary>
        public bool HasAdministrationPermissions() => AdministrationWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access favorites card links
        /// </summary>
        public bool HasFavoritesPermission() => FavoritesWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Indicates if the user has permission to access workbasket card links
        /// </summary>
        public bool HasWorkbasketPermission() => WorkbasketWidgetControl.HasPermissions() ? true : false;

        /// <summary>
        /// Update the graph showing the number of workbasket records assigned to the current user
        /// </summary>
        public void RefreshWorkbasketCard()
        {
            if (_workbasketControl != null && HomeForm.LoggedIn && ParentForm != null && ParentForm is HomeForm)
            {
                _workbasketControl.UpdateReport(ParentForm as HomeForm);
            }
        }

        /// <summary>
        /// Refresh the favorites card based on user card link selections
        /// </summary>
        public void RefreshFavorites()
        {
            if (_favoritesControl != null && HomeForm.LoggedIn)
            {
                _favoritesControl.RefreshFavorites();
            }
        }

        /// <summary>
        /// Clear the favorites links from the card
        /// </summary>
        public void ClearFavorites()
        {
            if (_favoritesControl != null && !_favoritesControl.IsDisposed)
                _favoritesControl.ClearFavorites();
        }

        /// <summary>
        /// Reset the cards to the default layout
        /// </summary>
        public void ResetDefaultCardLayout()
        {
            widgetView.BeginUpdate();

            _skipMessage = true;
            if (Workbasket.WorkbasketWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerWorkbasketCard)
                ShowCard(HomeTab.WorkbasketCardLabel);
            if (Favorites.FavoritesWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerFavoritesCard)
                ShowCard(HomeTab.FavoritesCardLabel);
            if (Members.MemberWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerMembersCard)
                ShowCard(HomeTab.MemberCardLabel);
            if (Claims.ClaimWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerClaimsCard)
                ShowCard(HomeTab.ClaimCardLabel);
            if (Providers.ProviderWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerProviderCard)
                ShowCard(HomeTab.ProviderCardLabel);
            if (Others.OtherWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerOthersCard)
                ShowCard(HomeTab.OtherCardLabel);
            if (Financials.FinancialWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerFinancialsCard)
                ShowCard(HomeTab.FinancialCardLabel);
            if (Utilization.UtilizationWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerUtilizationCard)
                ShowCard(HomeTab.UtilizationCardLabel);
            if (Funding.FundingWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerFundingCard)
                ShowCard(HomeTab.FundingCardLabel);
            if (Administration.AdministrationWidgetControl.HasPermissions() && !UserPreferenceUtil.Instance.UISettings.ShowManagerAdministrationCard)
                ShowCard(HomeTab.AdministrationCardLabel);
            _skipMessage = false;

            ResetCards();

            if (WorkbasketWidgetControl.HasPermissions())
            {
                if (documentWorkbasket.ControlName != null && _currentDocuments.Contains(documentWorkbasket.ControlName))
                    documentWorkbasket = _currentDocuments[documentWorkbasket.ControlName] as Document;
                documentWorkbasket.RowIndex = 0;
                documentWorkbasket.ColumnIndex = 0;

                documentWorkbasket.RowSpan = 1;
                documentWorkbasket.ColumnSpan = 1;
            }

            if (FavoritesWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerFavoritesCard = true;

                if (documentFavorites.ControlName != null && _currentDocuments.Contains(documentFavorites.ControlName))
                    documentFavorites = _currentDocuments[documentFavorites.ControlName] as Document;
                documentFavorites.RowIndex = 0;
                documentFavorites.ColumnIndex = 1;

                documentFavorites.RowSpan = 2;
                documentFavorites.ColumnSpan = 1;
            }

            if (MemberWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerMembersCard = true;

                if (documentMembers.ControlName != null && _currentDocuments.Contains(documentMembers.ControlName))
                    documentMembers = _currentDocuments[documentMembers.ControlName] as Document;
                documentMembers.RowIndex = 0;
                documentMembers.ColumnIndex = 2;

                documentMembers.RowSpan = 2;
                documentMembers.ColumnSpan = 1;
            }

            if (ClaimWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerClaimsCard = true;

                if (documentClaims.ControlName != null && _currentDocuments.Contains(documentClaims.ControlName))
                    documentClaims = _currentDocuments[documentClaims.ControlName] as Document;
                documentClaims.RowIndex = 1;
                documentClaims.ColumnIndex = 0;

                documentClaims.RowSpan = 1;
                documentClaims.ColumnSpan = 1;
            }

            if (ProviderWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerProviderCard = true;

                if (documentProviders.ControlName != null && _currentDocuments.Contains(documentProviders.ControlName))
                    documentProviders = _currentDocuments[documentProviders.ControlName] as Document;
                documentProviders.RowIndex = 2;
                documentProviders.ColumnIndex = 0;

                documentProviders.RowSpan = 1;
                documentProviders.ColumnSpan = 1;
            }

            if (OtherWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerOthersCard = true;

                if (documentOthers.ControlName != null && _currentDocuments.Contains(documentOthers.ControlName))
                    documentOthers = _currentDocuments[documentOthers.ControlName] as Document;
                documentOthers.RowIndex = 2;
                documentOthers.ColumnIndex = 1;

                documentOthers.RowSpan = 1;
                documentOthers.ColumnSpan = 1;
            }

            if (FinancialWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerFinancialsCard = true;

                if (documentFinancials.ControlName != null && _currentDocuments.Contains(documentFinancials.ControlName))
                    documentFinancials = _currentDocuments[documentFinancials.ControlName] as Document;
                documentFinancials.RowIndex = 2;
                documentFinancials.ColumnIndex = 2;

                documentFinancials.RowSpan = 1;
                documentFinancials.ColumnSpan = 1;
            }

            if (UtilizationWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerUtilizationCard = true;

                if (documentUtilization.ControlName != null && _currentDocuments.Contains(documentUtilization.ControlName))
                    documentUtilization = _currentDocuments[documentUtilization.ControlName] as Document;
                documentUtilization.RowIndex = 3;
                documentUtilization.ColumnIndex = 0;

                documentUtilization.RowSpan = 1;
                documentUtilization.ColumnSpan = 1;
            }

            if (FundingWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerFundingCard = true;

                if (documentFunding.ControlName != null && _currentDocuments.Contains(documentFunding.ControlName))
                    documentFunding = _currentDocuments[documentFunding.ControlName] as Document;
                documentFunding.RowIndex = 3;
                documentFunding.ColumnIndex = 1;

                documentFunding.RowSpan = 1;
                documentFunding.ColumnSpan = 1;
            }

            if (AdministrationWidgetControl.HasPermissions())
            {
                UserPreferenceUtil.Instance.UISettings.ShowManagerAdministrationCard = true;

                if (documentAdministration.ControlName != null && _currentDocuments.Contains(documentAdministration.ControlName))
                    documentAdministration = _currentDocuments[documentAdministration.ControlName] as Document;
                documentAdministration.RowIndex = 3;
                documentAdministration.ColumnIndex = 2;

                documentAdministration.RowSpan = 1;
                documentAdministration.ColumnSpan = 1;
            }

            widgetView.EndUpdate();
        }

        /// <summary>
        /// Store a list of 'empty' table cells
        /// </summary>
        public void InitializeRowColumnCells()
        {
            _skipMessage = false;
            _displayedMessage = false;
            _useDefaultLayout = true;

            _overlappingControls.Clear();
            _allCells.Clear();

            // Add every row and column combination
            for (int i = 0; i < widgetView.Rows.Count; i++)
            {
                for (int j = 0; j < widgetView.Columns.Count; j++)
                {
                    _allCells.Add(string.Concat(i, "x", j));
                }
            }

            // Remove any 'occupied' row and column combinations so we're left with 'empty' cells
            foreach (Document document in widgetView.Documents)
            {
                if (document.RowIndex > -1 && document.ColumnIndex > -1 && document.Control != null)
                {
                    string value = string.Concat(document.RowIndex, "x", document.ColumnIndex);
                    _allCells.Remove(string.Concat(document.RowIndex, "x", document.ColumnIndex));

                    if (document.RowSpan > 1)
                    {
                        for (int row = document.RowIndex; row < document.RowIndex + document.RowSpan; row++)
                        {
                            value = string.Concat(row, "x", document.ColumnIndex);
                            if (_allCells.Contains(value))
                                _allCells.Remove(value);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Restore the document layout by reading the saved layout file, if it exists
        /// </summary>
        public void RestoreDocumentLayout()
        {
            string fileName = UserPreferenceUtil.Instance.CardLayoutFilePath;
            if (string.IsNullOrEmpty(fileName))
                return;

            if (System.IO.File.Exists(fileName))
            {
                foreach (Document document in widgetView.Documents)
                    document.ColumnIndex = -1;

                _skipMessage = true;
                widgetView.SuspendLayout();
                documentManager.View.RestoreLayoutFromXml(fileName);
                documentManager.View.LayoutChanged();
                widgetView.ResumeLayout();
                _skipMessage = false;

                widgetView.DocumentProperties.AllowClose = true;

                foreach (Document document in widgetView.Documents)
                {
                    if (document.RowIndex == -1 || document.ColumnIndex == -1)
                    {
                        // These documents were closed by the user so set the corresponding
                        // program preference option
                        string caption = _documentLookup[document.ControlName] as string;
                        if (!string.IsNullOrEmpty(caption))
                            SetRemovedDocuments(document, caption);
                    }
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerClaimsCard || !ClaimWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentClaims, HomeTab.ClaimCardLabel);
                    if (widgetView.Documents.Contains(documentClaims))
                        widgetView.RemoveDocument(_claimControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerMembersCard || !MemberWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentMembers, HomeTab.MemberCardLabel);
                    if (widgetView.Documents.Contains(documentMembers))
                        widgetView.RemoveDocument(_memberControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerFinancialsCard || !FinancialWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentFinancials, HomeTab.FinancialCardLabel);
                    if (widgetView.Documents.Contains(documentFinancials))
                        widgetView.RemoveDocument(_financialControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerFundingCard || !FundingWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentFunding, HomeTab.FundingCardLabel);
                    if (widgetView.Documents.Contains(documentFunding))
                        widgetView.RemoveDocument(_fundingControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerOthersCard || !OtherWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentOthers, HomeTab.OtherCardLabel);
                    if (widgetView.Documents.Contains(documentOthers))
                        widgetView.RemoveDocument(_otherControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerProviderCard || !ProviderWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentProviders, HomeTab.ProviderCardLabel);
                    if (widgetView.Documents.Contains(documentProviders))
                        widgetView.RemoveDocument(_providerControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerUtilizationCard || !UtilizationWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentUtilization, HomeTab.UtilizationCardLabel);
                    if (widgetView.Documents.Contains(documentUtilization))
                        widgetView.RemoveDocument(_utilizationControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerAdministrationCard || !AdministrationWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentAdministration, HomeTab.AdministrationCardLabel);
                    if (widgetView.Documents.Contains(documentAdministration))
                        widgetView.RemoveDocument(_administrationControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerFavoritesCard || !FavoritesWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentFavorites, HomeTab.FavoritesCardLabel);
                    if (widgetView.Documents.Contains(documentFavorites))
                        widgetView.RemoveDocument(_favoritesControl);
                }

                if (!UserPreferenceUtil.Instance.UISettings.ShowManagerWorkbasketCard || !WorkbasketWidgetControl.HasPermissions())
                {
                    SetRemovedDocuments(documentWorkbasket, HomeTab.WorkbasketCardLabel);
                    if (widgetView.Documents.Contains(documentWorkbasket))
                        widgetView.RemoveDocument(_workbasketControl);
                }

                if ((!UserPreferenceUtil.Instance.UISettings.ShowManagerClaimsCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerMembersCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerFinancialsCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerFundingCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerOthersCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerProviderCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerUtilizationCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerAdministrationCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerFavoritesCard) &&
                    (!UserPreferenceUtil.Instance.UISettings.ShowManagerWorkbasketCard))
                {
                    foreach (Document document in widgetView.Documents)
                    {
                        document.RowIndex = -1;
                        document.ColumnIndex = -1;
                    }

                    InitializeRowColumnCells();

                    foreach (Document document in widgetView.Documents)
                    {
                        PlaceControl(document);
                    }

                    if (!_useDefaultLayout)
                    {
                        for (int i = 0; i < _overlappingControls.Count; i++)
                        {
                            Control control = _overlappingControls[i] as Control;
                            widgetView.ActivateDocument(control);
                        }
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initialize the refresh button image
        /// </summary>
        private void AddRefreshButton()
        {
            //if (HealthEdgeController.Instance == null)
            //    return;

            //System.Resources.ResourceManager rm = HealthEdgeController.Instance.ResourceMgr;
            //if (rm == null)
            //    return;

            //_refreshButton.Image = ((System.Drawing.Image)(rm.GetObject(Images.REFRESH_12)));
        }

        /// <summary>
        /// Remove the input document
        /// </summary>
        private void RemoveDocument(string caption)
        {
            //if (caption == ClaimCardLabel)
            //    widgetView.RemoveDocument(_claimControl);
            //else if (caption == MemberCardLabel)
            //    widgetView.RemoveDocument(_memberControl);
            

            //if (!string.IsNullOrEmpty(caption))
            //    SetRemovedDocuments(document, caption);
        }

        /// <summary>
        /// Don't show the context menu since we don't have card captions
        /// </summary>
        private void widgetView_PopupMenuShowing(object sender, DevExpress.XtraBars.Docking2010.Views.PopupMenuShowingEventArgs e)
        {
            e.Cancel = true;
        }

        /// <summary>
        /// Handle when the document selector is closed using Ctrl+Tab key combination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void widgetView_DocumentSelectorHidden(object sender, EventArgs e)
        {
            ChangeNames(false);
        }

        /// <summary>
        /// Create the cards for the home page
        /// </summary>
        private void widgetView_QueryControl(object sender, DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e)
        {
            widgetView.BeginUpdate();

            if (e.Document.ControlName == documentWorkbasket.ControlName)
            {
                if (_workbasketControl == null)
                {
                    _refreshButton.ToolTip = "Refresh the report";
                    _refreshButton.UseImage = true;
                    _refreshButton.UseCaption = false;
                    AddRefreshButton();
                    documentWorkbasket.CustomHeaderButtons.Add(_refreshButton);
                    _refreshButton.Style = DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton;
                    _refreshButton.GroupIndex = -1;
                    documentWorkbasket.CustomButtonClick += documentWorkbasket_CustomButtonClick;

                    _workbasketControl = new WorkbasketWidgetControl(this);
                    _widgetControls.Add(_workbasketControl);
                }

                e.Control = _workbasketControl;

                if (HomeForm.LoggedIn)
                {
                    if (UserPreferenceUtil.Instance.UISettings.ShowManagerWorkbasketCard)
                        _workbasketControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.WorkbasketCardLabel, documentWorkbasket);
                }
            }
            else if (e.Document.ControlName == documentMembers.ControlName)
            {
                if (_memberControl == null)
                {
                    _memberControl = new MemberWidgetControl(this);
                    _widgetControls.Add(_memberControl);
                }

                e.Control = _memberControl;

                if (HomeForm.LoggedIn)
                {
                    if (_memberControl != null && MemberWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerMembersCard)
                        _memberControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.MemberCardLabel, documentMembers);
                }
            }
            else if (e.Document.ControlName == documentProviders.ControlName)
            {
                if (_providerControl == null)
                {
                    _providerControl = new ProviderWidgetControl(this);
                    _widgetControls.Add(_providerControl);
                }

                e.Control = _providerControl;

                if (HomeForm.LoggedIn)
                {
                    if (_providerControl != null && ProviderWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerProviderCard)
                        _providerControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.ProviderCardLabel, documentProviders);
                }
            }
            else if (e.Document.ControlName == documentClaims.ControlName)
            {
                if (_claimControl == null)
                {
                    _claimControl = new ClaimWidgetControl(this);
                    _widgetControls.Add(_claimControl);
                }

                e.Control = _claimControl;

                if (HomeForm.LoggedIn)
                {
                    if (_claimControl != null && ClaimWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerClaimsCard)
                        _claimControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.ClaimCardLabel, documentClaims);
                }
            }
            else if (e.Document.ControlName == documentFinancials.ControlName)
            {
                if (_financialControl == null)
                {
                    _financialControl = new FinancialWidgetControl(this);
                    _widgetControls.Add(_financialControl);
                }

                e.Control = _financialControl;

                if (HomeForm.LoggedIn)
                {
                    if (_financialControl != null && FinancialWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerFinancialsCard)
                        _financialControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.FinancialCardLabel, documentFinancials);
                }
            }
            else if (e.Document.ControlName == documentFunding.ControlName)
            {
                if (_fundingControl == null)
                {
                    _fundingControl = new FundingWidgetControl(this);
                    _widgetControls.Add(_fundingControl);
                }

                e.Control = _fundingControl;

                if (HomeForm.LoggedIn)
                {
                    if (_fundingControl != null && FundingWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerFundingCard)
                        _fundingControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.FundingCardLabel, documentFunding);
                }
            }
            else if (e.Document.ControlName == documentUtilization.ControlName)
            {
                if (_utilizationControl == null)
                {
                    _utilizationControl = new UtilizationWidgetControl(this);
                    _widgetControls.Add(_utilizationControl);
                }

                e.Control = _utilizationControl;

                if (HomeForm.LoggedIn)
                {
                    if (_utilizationControl != null && UtilizationWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerUtilizationCard)
                        _utilizationControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.UtilizationCardLabel, documentUtilization);
                }
            }
            else if (e.Document.ControlName == documentOthers.ControlName)
            {
                if (_otherControl == null)
                {
                    _otherControl = new OtherWidgetControl(this);
                    _widgetControls.Add(_otherControl);
                }

                e.Control = _otherControl;

                if (HomeForm.LoggedIn)
                {
                    if (_otherControl != null && OtherWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerOthersCard)
                        _otherControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.OtherCardLabel, documentOthers);
                }
            }
            else if (e.Document.ControlName == documentAdministration.ControlName)
            {
                if (_administrationControl == null)
                {
                    _administrationControl = new AdministrationWidgetControl(this);
                    _widgetControls.Add(_administrationControl);
                }

                e.Control = _administrationControl;

                if (HomeForm.LoggedIn)
                {
                    if (_administrationControl != null && AdministrationWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerAdministrationCard)
                        _administrationControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.AdministrationCardLabel, documentAdministration);
                }
            }
            else if (e.Document.ControlName == documentFavorites.ControlName)
            {
                if (_favoritesControl == null)
                {
                    _favoritesControl = new FavoritesWidgetControl(this);
                    _widgetControls.Add(_favoritesControl);
                }

                e.Control = _favoritesControl;

                if (HomeForm.LoggedIn)
                {
                    if (_favoritesControl != null && FavoritesWidgetControl.HasPermissions() && UserPreferenceUtil.Instance.UISettings.ShowManagerFavoritesCard)
                        _favoritesControl.UpdateLinks();
                    else
                        RemoveDocument(HomeTab.FavoritesCardLabel, documentFavorites);
                }
            }

            widgetView.EndUpdate();
        }
        
        /// <summary>
        /// Save the card layout
        /// </summary>
        private void SaveLayout()
        {
            if (!documentManager.IsDisposing && documentManager.View != null)
            {
                using (System.IO.MemoryStream configuration = new System.IO.MemoryStream())
                {
                    string fileName = UserPreferenceUtil.Instance.CardLayoutFilePath;
                    if (string.IsNullOrEmpty(fileName))
                        return;

                    documentManager.View.SaveLayoutToXml(fileName);
                }
            }
        }

        /// <summary>
        /// Update the workbasket report when the refresh button is clicked
        /// </summary>
        private void documentWorkbasket_CustomButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button == _refreshButton)
            {
                if (_workbasketControl != null)
                {
                    HomeForm homeForm = ParentForm as HomeForm;
                    if (homeForm == null)
                        return;

                    homeForm.UpdateCvcCounts();
                    _workbasketControl.UpdateReport(homeForm);
                }
            }
        }

        /// <summary>
        /// Preserve the control's content and don't dispose of the document's control
        /// </summary>
        private void widgetView_ControlReleasing(object sender, DevExpress.XtraBars.Docking2010.Views.ControlReleasingEventArgs e)
        {
            e.DisposeControl = false;
            e.KeepControl = false;
        }

        /// <summary>
        /// Update the document's table cell position after the document is added to the view
        /// </summary>
        private void widgetView_DocumentAdded(object sender, DevExpress.XtraBars.Docking2010.Views.DocumentEventArgs e)
        {
            Document document = e.Document as Document;

            if (document != null)
            {
                if (document.ControlName != null)
                {
                    if (_currentDocuments.Contains(document.ControlName))
                        _currentDocuments.Remove(document.ControlName);
                    _currentDocuments.Add(document.ControlName, document);
                }
                else if (document.Control != null)
                {
                    string cardName = string.Empty;
                    if (document.Control is IWidgetControl)
                    {
                        cardName = (document.Control as IWidgetControl).WidgetControlName;
                    }

                    if (!string.IsNullOrEmpty(cardName))
                    {
                        if (_currentDocuments.Contains(cardName))
                            _currentDocuments.Remove(cardName);
                        _currentDocuments.Add(cardName, document);
                    }
                }

                PlaceControl(document);
            }
        }

        /// <summary>
        /// Update the removed document list after the document is closed
        /// </summary>
        private void widgetView_DocumentClosed(object sender, DevExpress.XtraBars.Docking2010.Views.DocumentEventArgs e)
        {
            Document document = e.Document as Document;

            string caption = string.Empty;
            if (!string.IsNullOrEmpty(document.ControlName))
            {
                if (_documentLookup.Contains(document.ControlName))
                    caption = _documentLookup[document.ControlName] as string;

                if (!string.IsNullOrEmpty(caption))
                    SetRemovedDocuments(document, caption);
            }
            else if (document.Control != null)
            {
                IWidgetControl control = document.Control as IWidgetControl;
                if (control != null)
                {
                    if (!string.IsNullOrEmpty(control.WidgetControlName))
                    {
                        if (_documentLookup.Contains(control.WidgetControlName))
                            caption = _documentLookup[control.WidgetControlName] as string;

                        if (!string.IsNullOrEmpty(caption))
                            SetRemovedDocuments(document, caption);
                    }
                }
            }
        }

        /// <summary>
        /// Show the input card
        /// </summary>
        private void ShowCard(string caption)
        {
            if (_removedDocuments.Contains(caption))
                _removedDocuments.Remove(caption);

            if (caption == ClaimCardLabel)
            {
                AddDocument(documentClaims.ControlName, caption);

                if (_claimControl != null)
                    widgetView.ActivateDocument(_claimControl);
            }
            else if (caption == MemberCardLabel)
            {
                AddDocument(documentMembers.ControlName, caption);

                if (_memberControl != null)
                    widgetView.ActivateDocument(_memberControl);
            }
            else if (caption == FinancialCardLabel)
            {
                AddDocument(documentFinancials.ControlName, caption);

                if (_financialControl != null)
                    widgetView.ActivateDocument(_financialControl);
            }
            else if (caption == ProviderCardLabel)
            {
                AddDocument(documentProviders.ControlName, caption);

                widgetView.ActivateDocument(_providerControl);
            }
            else if (caption == FundingCardLabel)
            {
                AddDocument(documentFunding.ControlName, caption);

                widgetView.ActivateDocument(_fundingControl);
            }
            else if (caption == OtherCardLabel)
            {
                AddDocument(documentOthers.ControlName, caption);

                widgetView.ActivateDocument(_otherControl);
            }
            else if (caption == UtilizationCardLabel)
            {
                AddDocument(documentUtilization.ControlName, caption);

                widgetView.ActivateDocument(_utilizationControl);
            }
            else if (caption == AdministrationCardLabel)
            {
                AddDocument(documentAdministration.ControlName, caption);

                widgetView.ActivateDocument(_administrationControl);
            }
            else if (caption == FavoritesCardLabel)
            {
                AddDocument(documentFavorites.ControlName, caption);

                widgetView.ActivateDocument(_favoritesControl);
            }
            else if (caption == WorkbasketCardLabel)
            {
                AddDocument(documentWorkbasket.ControlName, caption);

                widgetView.ActivateDocument(_workbasketControl);
            }
        }

        /// <summary>
        /// Add the document to the widget view
        /// </summary>
        private void AddDocument(string documentName, string caption)
        {
            Document doc = null;
            foreach (Document document in widgetView.Documents)
            {
                if (document.ControlName == documentName)
                {
                    doc = document;
                    break;
                }
            }

            if (doc == null)
            {
                if (_removedDocuments.Contains(caption))
                    _removedDocuments.Remove(caption);

                try
                {
                    widgetView.AddDocument(string.Empty, documentName);
                }
                catch { }
                return;
            }
        }

        /// <summary>
        /// Hide the input card
        /// </summary>
        private void HideCard(string caption, Document document)
        {
            if (!_removedDocuments.Contains(caption))
                _removedDocuments.Add(caption, document);

            if (document != null)
                RemoveDocument(caption, document);
        }

        /// <summary>
        /// Update the removed documents list and update the user preferences
        /// </summary>
        private void SetRemovedDocuments(Document document, string caption)
        {
            if (document != null && !_removedDocuments.Contains(caption))
                _removedDocuments.Add(caption, document);

            if (string.Compare(caption, ClaimCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerClaimsCard = false;

            if (string.Compare(caption, MemberCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerMembersCard = false;

            if (string.Compare(caption, FinancialCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerFinancialsCard = false;

            if (string.Compare(caption, ProviderCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerProviderCard = false;

            if (string.Compare(caption, OtherCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerOthersCard = false;

            if (string.Compare(caption, FundingCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerFundingCard = false;

            if (string.Compare(caption, UtilizationCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerUtilizationCard = false;

            if (string.Compare(caption, AdministrationCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerAdministrationCard = false;

            if (string.Compare(caption, FavoritesCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerFavoritesCard = false;

            if (string.Compare(caption, WorkbasketCardLabel, StringComparison.OrdinalIgnoreCase) == 0)
                UserPreferenceUtil.Instance.UISettings.ShowManagerWorkbasketCard = false;
        }

        /// <summary>
        /// Find the next 'available' table cell for the input document
        /// </summary>
        private void PlaceControl(Document document)
        {
            int rowIndex = -1;
            int colIndex = -1;

            GetDocumentPlacement(ref rowIndex, ref colIndex);

            if (rowIndex == -1 || colIndex == -1)
            {
                if (!_displayedMessage)
                    _displayedMessage = true;

                if (!_skipMessage)
                {
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("There are no available cells to place the remaining card(s).  Do you want to reset to the default card layout (Yes) or manually move the cards (No)?\r\n\r\nIf you choose to manually move the cards, the cards will overlap in the upper left hand corner of the screen.", "Card Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        _useDefaultLayout = false;
                        document.RowIndex = 0;
                        document.ColumnIndex = 0;

                        if (document.Control != null)
                        {
                            if (!_overlappingControls.Contains(document.Control))
                                _overlappingControls.Add(document.Control);
                        }
                    }
                }
            }
            else
            {
                document.RowIndex = rowIndex;
                document.ColumnIndex = colIndex;
            }
        }

        /// <summary>
        /// Reset the card indexes and span before resetting to the default layout
        /// </summary>
        private void ResetCards()
        {
            documentWorkbasket.RowIndex = -1;
            documentWorkbasket.ColumnIndex = -1;
            documentWorkbasket.RowSpan = -1;
            documentWorkbasket.ColumnSpan = -1;

            documentFavorites.RowIndex = -1;
            documentFavorites.ColumnIndex = -1;
            documentFavorites.RowSpan = -1;
            documentFavorites.ColumnSpan = -1;

            documentMembers.RowIndex = -1;
            documentMembers.ColumnIndex = -1;
            documentMembers.RowSpan = -1;
            documentMembers.ColumnSpan = -1;

            documentClaims.RowIndex = -1;
            documentClaims.ColumnIndex = -1;
            documentClaims.RowSpan = -1;
            documentClaims.ColumnSpan = -1;

            documentProviders.RowIndex = -1;
            documentProviders.ColumnIndex = -1;
            documentProviders.RowSpan = -1;
            documentProviders.ColumnSpan = -1;

            documentOthers.RowIndex = -1;
            documentOthers.ColumnIndex = -1;
            documentOthers.RowSpan = -1;
            documentOthers.ColumnSpan = -1;

            documentFinancials.RowIndex = -1;
            documentFinancials.ColumnIndex = -1;
            documentFinancials.RowSpan = -1;
            documentFinancials.ColumnSpan = -1;

            documentUtilization.RowIndex = -1;
            documentUtilization.ColumnIndex = -1;
            documentUtilization.RowSpan = -1;
            documentUtilization.ColumnSpan = -1;

            documentFunding.RowIndex = -1;
            documentFunding.ColumnIndex = -1;
            documentFunding.RowSpan = -1;
            documentFunding.ColumnSpan = -1;

            documentAdministration.RowIndex = -1;
            documentAdministration.ColumnIndex = -1;
            documentAdministration.RowSpan = -1;
            documentAdministration.ColumnSpan = -1;
        }

        /// <summary>
        /// Get the next available 'slot' in the table layout
        /// </summary>
        private void GetDocumentPlacement(ref int rowIndex, ref int colIndex)
        {
            if (_allCells.Count > 0)
            {
                string cell = (string)_allCells[0];
                int index = cell.IndexOf("x");
                if (index != -1)
                {
                    string row = cell.Substring(0, index);
                    string col = cell.Substring(index + 1, 1);
                    rowIndex = int.Parse(row);
                    colIndex = int.Parse(col);
                    _allCells.RemoveAt(0);
                }
            }
        }

        #endregion       
    }
}
