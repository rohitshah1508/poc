/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using HealthEdge.Framework;
using HealthEdge.Controller;
using HealthEdge.Controls;
using HealthEdge.Inquiry.SearchResult;
using HealthEdge.Entities;
using HealthEdge.Entities.Generated;
using HealthEdge.SecurityAndAdministration;


namespace HealthEdge.Manager.Controls
{
    public partial class TaskRow : XtraUserControl
    {
        private Image _image;
        private List<TaskLink> _tasks;
        private string _text = string.Empty;

        static TaskRow() { }

        public TaskRow()
        {
            InitializeComponent();

            pictureBoxImage.Visible = false;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_tasks?.Count > 0)
                {
                    foreach (Control control in panelControlTaskLinks.Controls)
                    {
                        if (control is SimpleButton)
                        {
                            SimpleButton button = control as SimpleButton;
                            button.Click -= ButtonEdit_Click;
                            button.Tag = null;
                            button.Image?.Dispose();
                            button.Image = null;
                        }
                    }
                }

                components?.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Properties

        [Browsable(true)]
        public Image Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                pictureBoxImage.Image = _image;
                pictureBoxImage.Visible = false;
            }
        }

        [Browsable(true)]
        [DefaultValue("")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public new string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                labelControl.Text = _text;
                labelControl.ForeColor = DevExpress.Utils.Frames.FrameHelper.IsDarkSkin(LookAndFeel) ? Utility.Winforms.SkinUtility.ForegroundColor : Color.FromArgb(99, 99, 99);
            }
        }


        [Browsable(true)]
        public List<TaskLink> Tasks
        {
            get
            {
                return _tasks;
            }
            set
            {
                _tasks = value;
            }
        }

        #endregion

        #region Public Methods

        public void UpdateTaskLinks()
        {
            if (_tasks == null)
                return;

            foreach (Control control in panelControlTaskLinks.Controls)
            {
                if (control is SimpleButton)
                {
                    SimpleButton simpleButton = control as SimpleButton;
                    simpleButton.Click -= ButtonEdit_Click;
                    simpleButton.Tag = null;
                }
            }

            panelControlTaskLinks.Controls.Clear();

            SuspendLayout();

            int xPos = 2;
            int yPos = 7;
            for (int i = 0; i < _tasks.Count; i++)
            {
                SimpleButton link = new SimpleButton();

                link.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
                link.AllowFocus = true;
                link.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
                link.Image = _tasks[i].Image;
                link.ImageLocation = ImageLocation.MiddleCenter;
                link.TabIndex = i;
                link.Tag = _tasks[i];
                link.Location = new Point(xPos, yPos);
                link.Name = link.GetType().Name + _tasks[i].Text;
                link.Size = new System.Drawing.Size(16, 16);
                link.ToolTip = _tasks[i].Text;
                xPos += 35;

                if (!HealthEdge.Utility.WinForms.DesignMode)
                    link.Click += ButtonEdit_Click;

                panelControlTaskLinks.Controls.Add(link);
            }

            ResumeLayout();
        }

        #endregion

        #region Private Methods

        private void Tasks_ListChanged(object sender, ListChangedEventArgs e)
        {
            UpdateTaskLinks();
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            bool closePopup = false;
            HomeForm home = null;

            if (ParentForm != null && ParentForm is HomeForm)
            {
                home = this.ParentForm as HomeForm;
            }
            else if (ParentForm != null && ParentForm.Owner != null && ParentForm.Owner is HomeForm)
            {
                home = this.ParentForm.Owner as HomeForm;
                closePopup = true;
            }

            TaskLink task = (sender as SimpleButton).Tag as TaskLink;
            if (task != null)
            {
                if (closePopup && home != null)
                    home.HidePopup();

                switch (task.Verb)
                {
                    case "BPWizard":
                        HealthEdge.Controls.HealthRules.BPWizard.BenefitPlanWizardForm wizard = new HealthEdge.Controls.HealthRules.BPWizard.BenefitPlanWizardForm();
                        wizard.Show();
                        break;
                    case "Find":
                        FindAndDisplayEntity(home, task);
                        break;
                    case "Create":
                    case "CreatePremiumRefund":
                        CreateEntity(home, task, sender);
                        break;
                    case "CreateBatch":
                        if (home != null)
                            home.CreateBatch(task.SearchType);
                        break;
                    case "EnterBatch":
                        if (home != null)
                            home.EnterBatch(task.SearchType);
                        break;
                    case AdjustmentPayable.TYPE_NAME:
                        if (home != null)
                            home.barButtonItemAdjustmentPayable_ItemClick(null, null);
                        break;
                    case ManualReceivable.TYPE_NAME:
                        if (home != null)
                            home.barButtonItemManualReceivable_ItemClick(null, null);
                        break;
                    case RecoupmentPayment.TYPE_NAME:
                        if (task.Text == "Create")
                        {
                            if (home != null)
                                home.barButtonItemRecoupmentPayment_ItemClick(null, null);
                        }
                        break;
                    case "PaymentRun":
                        if (home != null)
                            home.barButtonItemTriggerPaymentRun_ItemClick(null, null);
                        break;
                    case "VoidCheck":
                        if (home != null)
                            home.barButtonItemVoidCheck_ItemClick(null, null);
                        break;
                    case "DataMaintenance":
                        if (home != null)
                            home.barButtonItemDataMaintenance_ItemClick(null, null);
                        break;
                    case "LockManagement":
                        if (home != null)
                            home.barButtonItemLockManager_ItemClick(null, null);
                        break;
                    case "TransactionManagement":
                        if (home != null)
                            home.barButtonItemTransactionManagement_ItemClick(null, null);
                        break;
                    case "BatchTransaction":
                        if (home != null)
                            home.barButtonItemBatchTransaction_ItemClick(null, null);
                        break;
                    case "RequestReprocessing":
                        HealthEdge.Controls.Reprocessing.RequestReprocessingWizard.RunRequestReprocessingWizard();
                        break;
                    case "PerfMeasures":
                        // TODO: Check security attributes
                        if (home != null)
                            home.togglePerformanceMeasurements_ItemClick(null, null);
                        // Toggle the label accordingly (enable vs. disable)
                        break;
                    case "DisplayPerfMeasures":
                        // TODO: Check security attributes
                        if (home != null)
                            home.displayPerformanceMeasurements_ItemClick(null, null);
                        break;
                    case "ViewPayments":
                        if (home != null)
                            home.barButtonItemSearchPayments_ItemClick(null, null);
                        break;
                    case "BenefitPredictor":
                        if (home != null)
                            home.barButtonItemBenefitSearchEditor_ItemClick(null, null);
                        break;
                    case "TrialClaim":
                        if (home != null)
                            home.barButtonItemTrialClaimQuestionEditor_ItemClick(null, null);
                        break;
                }
            }
        }

        private bool DisableLink(TaskLink taskLink)
        {
            string typeName = taskLink.EntityType;
            string verb = taskLink.Verb;
            if (typeName == "Member") typeName = "Subscription";
            if (typeName == "ServiceAuthorization") typeName = "InteractiveServiceAuthorization";

            bool disable = false;
            switch (verb)
            {
                case "BPWizard":
                    return !(SecurityCache.UserIsAuthorizedToPerformActionOnType(BenefitPlan.TYPE_NAME, TypeAction.WizardCreate) ||
                        SecurityCache.UserIsAuthorizedToPerformActionOnType(BenefitPlan.TYPE_NAME, TypeAction.WizardAmend));

                case "Create":
                    if (Equals(HCFA1500.TYPE_NAME, typeName) || Equals(UB92.TYPE_NAME, typeName) ||
                       (Equals(DentalSupplierInvoice.TYPE_NAME, typeName)))
                    {
                        disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.Enter);
                    }
                    else
                    {
                        disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(typeName, TypeAction.Create);
                    }

                    if (Equals(ServiceRequest.TYPE_NAME, typeName))
                    {
                        disable = !(SecurityCache.UserIsAuthorizedToPerformActionOnType(ServiceRequest.TYPE_NAME, TypeAction.Create) &&
                                    SecurityCache.UserIsAuthorizedToPerformActionOnType(Issue.TYPE_NAME, TypeAction.View));
                    }

                    break;

                case "CreateBatch":
                    disable = !(SecurityCache.UserIsAuthorizedToPerformActionOnType(Entities.Generated.ClaimBatch.TYPE_NAME, TypeAction.Create));

                    if (!disable)
                    {
                        if (Equals(HCFA1500.TYPE_NAME, typeName))
                        {
                            disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.EnterProfessionalClaimBatch);
                        }
                        else if (Equals(UB92.TYPE_NAME, typeName))
                        {
                            disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.EnterInstitutionalClaimBatch);
                        }
                        else if (Equals(DentalSupplierInvoice.TYPE_NAME, typeName))
                        {
                            disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.EnterDentalClaimBatch);
                        }
                    }
                    break;

                case "EnterBatch":
                    disable = !(SecurityCache.UserIsAuthorizedToPerformActionOnType(Entities.Generated.ClaimBatch.TYPE_NAME, TypeAction.View)
                        && SecurityCache.UserIsAuthorizedToPerformActionOnType(Entities.Generated.ClaimBatch.TYPE_NAME, TypeAction.Amend));

                    if (!disable)
                    {
                        if (Equals(HCFA1500.TYPE_NAME, typeName))
                        {
                            disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.EnterProfessionalClaimBatch);
                        }
                        else if (Equals(UB92.TYPE_NAME, typeName))
                        {
                            disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.EnterInstitutionalClaimBatch);
                        }
                        else if (Equals(DentalSupplierInvoice.TYPE_NAME, typeName))
                        {
                            disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.EnterDentalClaimBatch);
                        }
                    }
                    break;

                case "Find":
                    if (Equals(ConsolidatedClaim.TYPE_NAME, typeName) || Equals("Claim", typeName))
                    {
                        disable = !SecurityCache.UserIsAuthorizedToProcessClaims(ClaimAction.View);
                    }
                    else
                    {
                        disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(typeName, TypeAction.View);
                    }
                    break;

                case "ViewPayments":
                    disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(Payment.TYPE_NAME, TypeAction.View);
                    break;

                case "PaymentRun":
                    disable = !(SecurityCache.UserIsAuthorizedToPerformActionOnType(PaymentRunDefinition.TYPE_NAME, TypeAction.Create) &&
                             SecurityCache.UserIsAuthorizedToPerformActionOnType(CodeEntry.TYPE_NAME, TypeAction.View) &&
                             SecurityCache.UserIsAuthorizedToPerformActionOnType(PaymentCycle.TYPE_NAME, TypeAction.View) &&
                             SecurityCache.UserIsAuthorizedToPerformActionOnType(PaymentCycle.TYPE_NAME, TypeAction.Execute));
                    break;

                case AdjustmentPayable.TYPE_NAME:
                    disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(AdjustmentPayable.TYPE_NAME, TypeAction.Create);
                    break;

                case ManualReceivable.TYPE_NAME:
                    disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(Receivable.TYPE_NAME, TypeAction.Create);
                    break;

                case RecoupmentPayment.TYPE_NAME:
                    disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(RecoupmentPayment.TYPE_NAME, TypeAction.Create);
                    break;

                case "VoidCheck":
                    disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(Payment.TYPE_NAME, TypeAction.Amend);
                    break;

                case "LockManagement":
                case "TransactionManagement":
                    disable = !SecurityCache.UserIsTransactAdmin();
                    break;

                case "BatchTransaction":
                    disable = !SecurityCache.UserIsAuthorizedToPerformActionOnType(SubscriptionBatchChange.TYPE_NAME, TypeAction.View);
                    break;
            }

            return disable;
        }

        private void link_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            bool closePopup = false;
            HomeForm home = null;

            if (ParentForm != null && ParentForm is HomeForm)
            {
                home = this.ParentForm as HomeForm;
            }
            else if (ParentForm != null && ParentForm.Owner is HomeForm)
            {
                home = this.ParentForm.Owner as HomeForm;
                closePopup = true;
            }

            TaskLink task = (sender as SimpleButton).Tag as TaskLink;
            if (task != null)
            {
                if (closePopup && home != null)
                    home.HidePopup();

                switch (task.Verb)
                {
                    case "BPWizard":
                        HealthEdge.Controls.HealthRules.BPWizard.BenefitPlanWizardForm wizard = new HealthEdge.Controls.HealthRules.BPWizard.BenefitPlanWizardForm();
                        wizard.Show();
                        break;

                    case "Find":
                        FindAndDisplayEntity(home, task);
                        break;

                    case "Create":
                        CreateEntity(home, task, sender);
                        break;

                    case "CreateBatch":
                        if (home != null)
                            home.CreateBatch(task.SearchType);
                        break;

                    case "EnterBatch":
                        if (home != null)
                            home.EnterBatch(task.SearchType);
                        break;

                    case AdjustmentPayable.TYPE_NAME:
                        if (home != null)
                            home.barButtonItemAdjustmentPayable_ItemClick(null, null);
                        break;

                    case ManualReceivable.TYPE_NAME:
                        if (home != null)
                            home.barButtonItemManualReceivable_ItemClick(null, null);
                        break;

                    case RecoupmentPayment.TYPE_NAME:
                        if (task.Text == "Create")
                        {
                            if (home != null)
                                home.barButtonItemRecoupmentPayment_ItemClick(null, null);
                        }
                        break;

                    case "PaymentRun":
                        if (home != null)
                            home.barButtonItemTriggerPaymentRun_ItemClick(null, null);
                        break;

                    case "VoidCheck":
                        if (home != null)
                            home.barButtonItemVoidCheck_ItemClick(null, null);
                        break;

                    case "DataMaintenance":
                        if (home != null)
                            home.barButtonItemDataMaintenance_ItemClick(null, null);
                        break;

                    case "LockManagement":
                        if (home != null)
                            home.barButtonItemLockManager_ItemClick(null, null);
                        break;

                    case "TransactionManagement":
                        if (home != null)
                            home.barButtonItemTransactionManagement_ItemClick(null, null);
                        break;

                    case "BatchTransaction":
                        if (home != null)
                            home.barButtonItemBatchTransaction_ItemClick(null, null);
                        break;

                    case "PerfMeasures":
                        // TODO: Check security attributes
                        if (home != null)
                            home.togglePerformanceMeasurements_ItemClick(null, null);
                        // Toggle the label accordingly (enable vs. disable)
                        break;

                    case "DisplayPerfMeasures":
                        // TODO: Check security attributes
                        if (home != null)
                            home.displayPerformanceMeasurements_ItemClick(null, null);
                        break;

                    case "ViewPayments":
                        if (home != null)
                            home.barButtonItemSearchPayments_ItemClick(null, null);
                        break;

                    case "BenefitPredictor":
                        if (home != null)
                            home.barButtonItemBenefitSearchEditor_ItemClick(null, null);
                        break;

                    case "TrialClaim":
                        if (home != null)
                            home.barButtonItemTrialClaimQuestionEditor_ItemClick(null, null);
                        break;
                }
            }
        }


        private void CreateEntity(HomeForm home, TaskLink t, object sender)
        {
            if (t == null || home == null)
                return;

            DocumentType documentType = (DocumentType)Enum.Parse(typeof(DocumentType), t.EntityType);

            switch (documentType)
            {
                case DocumentType.UB92:
                    home.enterUB_ItemClick(null, null);
                    break;

                case DocumentType.HCFA1500:
                    home.enterHcfa_ItemClick(null, null);
                    break;

                case DocumentType.DentalSupplierInvoice:
                    home.enterDental_ItemClick(null, null);
                    break;

                case DocumentType.ServiceRequest:
                    home.CreateItem(Controller.SearchType.SERVICEREQUEST_SEARCH);
                    break;

                case DocumentType.PremiumPaymentBatchInput:
                    home.CreateItem(Controller.SearchType.PREMIUM_PAYMENT_BATCH_SEARCH);
                    break;

                case DocumentType.PremiumRefund:
                    home.CreateItem(Controller.SearchType.PREMIUM_REFUND_SEARCH);
                    break;

                default:
                    HealthEdge.Controller.SearchType searchType = (HealthEdge.Controller.SearchType)GetSearchType(t.SearchType.ToString());
                    home.createItemTaskRowHandler(searchType);
                    break;
            }
        }

        private void FindAndDisplayEntity(HomeForm home, TaskLink t)
        {
            if (t == null || home == null) 
                return;

            object searchResultRow = null;
            HealthEdge.Controller.SearchType searchType;
            Object searchResult = null;
            SearchInput searchInput = null;
            SearchDialog dlg = null;
            int focusedRowHandle = -1;
            SelectedCommand command = SearchDialog.Search("Manager", t.EntityType, false, false, out searchResultRow, out searchResult, ref searchInput, out searchType, out dlg, ref focusedRowHandle);

            switch (command)
            {
                case SelectedCommand.View:
                    ShowItem(searchType, searchResultRow, searchResult);
                    break;

                case SelectedCommand.Create:
                    if (t.SearchType == UB92.TYPE_NAME)
                    {
                        home.enterUB_ItemClick(null, null);
                    }
                    else if (t.SearchType == HCFA1500.TYPE_NAME)
                    {
                        home.enterHcfa_ItemClick(null, null);
                    }
                    else
                    {
                        home.createItemTaskRowHandler(searchType);
                    }
                    break;

                case SelectedCommand.Edit:
                    EditItem(searchType, searchResultRow, searchResult);
                    break;
            }
        }

        public static void ShowItem(HealthEdge.Controller.SearchType searchType, object searchRow, object searchResultRow)
        {
            switch (searchType.DocumentType)
            {
                case DocumentType.Account:
                    DetailsFactory.ShowPurchaser(searchRow);
                    break;

                case DocumentType.AccumulatorAdjustment:
                    DetailsFactory.ShowAccumulatorAdjustment(searchRow);
                    break;

                case DocumentType.Attachment:
                    DetailsFactory.ShowAttachment(searchRow);
                    break;

                case DocumentType.BankAccount:
                    DetailsFactory.ShowBankAccount(searchRow);
                    break;

                case DocumentType.BillingAdjustment:
                    DetailsFactory.ShowBillingAdjustment(searchRow);
                    break;

                case DocumentType.BillingRateComponent:
                    DetailsFactory.ShowBillingRateComponent(searchRow);
                    break;

                case DocumentType.BillingCycle:
                    DetailsFactory.ShowBillingCycle(searchRow);
                    break;

                case DocumentType.Broker:
                    DetailsFactory.ShowBroker(searchRow);
                    break;

                case DocumentType.BrokerAgent:
                    DetailsFactory.ShowBrokerAgent(searchRow);
                    break;

                case DocumentType.BrokerContract:
                    DetailsFactory.ShowBrokerContract(searchRow);
                    break;

                case DocumentType.EEBusinessCalendar:
                    DetailsFactory.ShowBusinessCalendar(searchRow);
                    break;

                case DocumentType.Carrier:
                    DetailsFactory.ShowCarrier(searchRow);
                    break;

                case DocumentType.UB92:
                case DocumentType.HCFA1500:
                case DocumentType.Claim:
                    DetailsFactory.ShowClaim(searchRow, searchResultRow as IDocument);
                    break;

                case DocumentType.ComplianceProgram:
                    DetailsFactory.ShowComplianceProgram(searchRow);
                    break;
                case DocumentType.ExternalMember:
                    DetailsFactory.ShowExternalMember(searchRow);
                    break;

                case DocumentType.FundingPayable:
                    DetailsFactory.ShowFundingPayable(searchRow);
                    break;

                case DocumentType.FundingPayment:
                    DetailsFactory.ShowFundingPayment(searchRow);
                    break;
                case DocumentType.FundingReceivable:
                    DetailsFactory.ShowFundingReceivable(searchRow);
                    break;

                case DocumentType.FundingRequestAdjustment:
                    DetailsFactory.ShowFundingRequestAdjustment(searchRow);
                    break;

                case DocumentType.Issue:
                    DetailsFactory.ShowIssue(searchRow);
                    break;

                case DocumentType.LienHolder:
                    DetailsFactory.ShowLienHolder(searchRow);
                    break;

                case DocumentType.Member:
                    DetailsFactory.ShowMember(searchRow);
                    break;

                case DocumentType.MemberAccount:
                    DetailsFactory.ShowMemberAccount(searchRow);
                    break;

                case DocumentType.MemberHealthCondition:
                    DetailsFactory.ShowMemberHealthCondition(searchRow);
                    break;

                case DocumentType.NamedProviderGrouping:
                    DetailsFactory.ShowNamedProviderGrouping(searchRow);
                    break;

                case DocumentType.OtherBillRecipient:
                    DetailsFactory.ShowOtherBillRecipient(searchRow);
                    break;

                case DocumentType.Payable:
                    PayableRow payableRow = searchRow as PayableRow;
                    if (payableRow.Type == "Manual")
                        DetailsFactory.ShowAdjustmentPayable(searchRow);
                    else if (payableRow.Type == "System")
                        DetailsFactory.ShowClaimPayable(searchRow);
                    break;

                case DocumentType.Payment:
                    DetailsFactory.ShowPayment(searchRow);
                    break;

                case DocumentType.PaymentCycle:
                    DetailsFactory.ShowPaymentCycle(searchRow);
                    break;

                case DocumentType.Practitioner:
                    DetailsFactory.ShowPractitioner(searchRow);
                    break;

                case DocumentType.PremiumPayment:
                    DetailsFactory.ShowPremiumPayment(searchRow);
                    break;

                case DocumentType.PremiumRefund:
                    DetailsFactory.ShowPremiumRefund(searchRow as PremiumRefundRow);
                    break;

                case DocumentType.PremiumPaymentBatchInput:
                    DetailsFactory.ShowPremiumPaymentBatchInput(searchRow as PremiumPaymentBatchRow);
                    break;

                case DocumentType.QualityAdjustmentDetail:
                    DetailsFactory.ShowQualityAdjustmentDetail(searchRow);
                    break;

                case DocumentType.QualityAdjustmentTable:
                    DetailsFactory.ShowQualityAdjustmentTable(searchRow);
                    break;

                case DocumentType.Receivable:
                    DetailsFactory.ShowReceivable(searchRow);
                    break;

                case DocumentType.RecoupmentPayment:
                    DetailsFactory.ShowRecoupmentPayment(searchRow);
                    break;

                case DocumentType.Reinsurer:
                    DetailsFactory.ShowReinsurer(searchRow);
                    break;

                case DocumentType.ReinsurerContract:
                    DetailsFactory.ShowReinsurerContract(searchRow);
                    break;

                case DocumentType.ServiceAuthorization:
                    DetailsFactory.ShowServiceAuthorization(searchRow);
                    break;

                case DocumentType.SharedBillingRate:
                    DetailsFactory.ShowSharedBillingRate(searchRow);
                    break;

                case DocumentType.Supplier:
                    DetailsFactory.ShowSupplier(searchRow);
                    break;

                case DocumentType.SupplierLocation:
                    DetailsFactory.ShowSupplierLocation(searchRow);
                    break;

                case DocumentType.SupplierNetwork:
                    DetailsFactory.ShowSupplierNetwork(searchRow);
                    break;

                case DocumentType.TaxEntity:
                    DetailsFactory.ShowTax(searchRow);
                    break;

                case DocumentType.UserAccount:
                    DetailsFactory.ShowUserAccount(searchRow);
                    break;

                case DocumentType.WithholdArrangement:
                    DetailsFactory.ShowWithholdArrangement(searchRow);
                    break;
            }
        }

        public static void EditItem(HealthEdge.Controller.SearchType searchType, object searchRow, object searchResultRow)
        {
            switch (searchType.DocumentType)
            {
                case DocumentType.Account:
                    DetailsFactory.EditPurchaser(searchRow);
                    break;

                case DocumentType.AccumulatorAdjustment:
                    DetailsFactory.EditAccumulatorAdjustment(searchRow);
                    break;

                case DocumentType.BankAccount:
                    DetailsFactory.EditBankAccount(searchRow);
                    break;
                case DocumentType.BillingRateComponent:
                    DetailsFactory.EditBillingRateComponent(searchRow);
                    break;

                case DocumentType.Broker:
                    DetailsFactory.EditBroker(searchRow);
                    break;

                case DocumentType.BrokerAgent:
                    DetailsFactory.EditBrokerAgent(searchRow);
                    break;

                case DocumentType.BrokerContract:
                    DetailsFactory.EditBrokerContract(searchRow);
                    break;

                case DocumentType.EEBusinessCalendar:
                    DetailsFactory.EditBusinessCalendar(searchRow);
                    break;

                case DocumentType.BillingCycle:
                    DetailsFactory.EditBillingCycle(searchRow);
                    break;

                case DocumentType.Carrier:
                    DetailsFactory.EditCarrier(searchRow);
                    break;

                case DocumentType.ComplianceProgram:
                    DetailsFactory.EditComplianceProgram(searchRow);
                    break;

                case DocumentType.ExternalMember:
                    ExternalMemberRow row = searchRow as ExternalMemberRow;

                    if (row == null || row.SystemId == null) return;

                    HealthEdge.Controls.ExternalMember.ExternalMemberView.EditExternalMember(row.SystemId, Entity.CURRENT_ENDORSEMENT);
                    break;

                case DocumentType.Issue:
                    DetailsFactory.EditIssue(searchRow);
                    break;

                case DocumentType.UB92:
                case DocumentType.HCFA1500:
                case DocumentType.Claim:
                    ClaimRow claimRow = searchRow as ClaimRow;
                    if (claimRow == null)
                        return;

                    // Check if there are any security rules in place preventing users from editing the claim
                    if (SecurityAdminWrapper.UserCanAmendClaim(claimRow.ClaimSystemId))
                    {
                        DetailsFactory.EditClaim(searchRow);
                    }
                    else
                    {
                        string message = string.Format("You do not have permission to edit this claim");
                        XtraMessageBox.Show(message, "Security Permission", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;

                case DocumentType.LienHolder:
                    DetailsFactory.EditLienHolder(searchRow);
                    break;

                case DocumentType.Member:
                    DetailsFactory.EditMember(searchRow);
                    break;

                case DocumentType.MemberAccount:
                    DetailsFactory.EditMemberAccount(searchRow);
                    break;

                case DocumentType.MemberHealthCondition:
                    DetailsFactory.EditMemberHealthCondition(searchRow);
                    break;

                case DocumentType.NamedProviderGrouping:
                    DetailsFactory.EditNamedProviderGrouping(searchRow);
                    break;

                case DocumentType.OtherBillRecipient:
                    DetailsFactory.EditOtherBillRecipient(searchRow);
                    break;

                case DocumentType.PaymentCycle:
                    DetailsFactory.EditPaymentCycle(searchRow);
                    break;

                case DocumentType.Practitioner:
                    DetailsFactory.EditPractitioner(searchRow);
                    break;

                case DocumentType.Reinsurer:
                    DetailsFactory.EditReinsurer(searchRow);
                    break;

                case DocumentType.ReinsurerContract:
                    DetailsFactory.EditReinsurerContract(searchRow);
                    break;

                case DocumentType.ServiceAuthorization:
                    ServiceAuthorizationRow authRow = searchRow as ServiceAuthorizationRow;
                    if (authRow == null)
                        return;

                    bool isTransientAuthorization = !String.IsNullOrEmpty(authRow.WorkbasketState) ? true : false;

                    // Check if there are any security rules in place preventing users from editing the service authorization
                    if (SecurityAdminWrapper.UserCanAmendISA(authRow.SystemId, isTransientAuthorization))
                    {
                        DetailsFactory.EditServiceAuthorization(searchRow);
                    }
                    else
                    {
                        string message = string.Format("You do not have permission to edit this service authorization");
                        XtraMessageBox.Show(message, "Security Permission", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;

                case DocumentType.SharedBillingRate:
                    DetailsFactory.EditSharedBillingRate(searchRow);
                    break;

                case DocumentType.Supplier:
                    DetailsFactory.EditSupplier(searchRow);
                    break;

                case DocumentType.SupplierLocation:
                    DetailsFactory.EditSupplierLocation(searchRow);
                    break;

                case DocumentType.SupplierNetwork:
                    DetailsFactory.EditSupplierNetwork(searchRow);
                    break;

                case DocumentType.TaxEntity:
                    DetailsFactory.EditTax(searchRow);
                    break;

                case DocumentType.UserAccount:
                    DetailsFactory.EditUserAccount(searchRow);
                    break;

                case DocumentType.WithholdArrangement:
                    DetailsFactory.EditWithholdArrangement(searchRow);
                    break;
            }
        }

        private SearchType GetSearchType(string name)
        {
            foreach (SearchType searchType in SearchType.Manager_SearchTypes)
            {
                string caption = searchType.Caption.IndexOf('&') > -1 ? searchType.Caption.Remove(searchType.Caption.IndexOf('&'), 1) : searchType.Caption;
                if (caption.Equals(name))
                {
                    return searchType as SearchType;
                }
            }

            return null;
        }

        #endregion
    }
}
