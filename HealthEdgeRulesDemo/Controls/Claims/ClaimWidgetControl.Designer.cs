/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
namespace HealthEdgeRulesDemo.Controls.Claims
{
    partial class ClaimWidgetControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClaimWidgetControl));
            this.xtraScrollableControlClaims = new System.Windows.Forms.ScrollableControl();
            this.hyperlinkLabelControlShowClaimLinks = new System.Windows.Forms.Label();
            this.panelControl = new System.Windows.Forms.Panel();
            this.pictureBoxClaims = new System.Windows.Forms.PictureBox();
            this.labelControlClaims = new System.Windows.Forms.Label();
            this.xtraScrollableControlClaims.SuspendLayout();
            this.panelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClaims)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControlClaims
            // 
            this.xtraScrollableControlClaims.Controls.Add(this.hyperlinkLabelControlShowClaimLinks);
            this.xtraScrollableControlClaims.Controls.Add(this.panelControl);
            this.xtraScrollableControlClaims.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControlClaims.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControlClaims.Name = "xtraScrollableControlClaims";
            this.xtraScrollableControlClaims.Size = new System.Drawing.Size(240, 200);
            this.xtraScrollableControlClaims.TabIndex = 0;
            // 
            // hyperlinkLabelControlShowClaimLinks
            // 
            this.hyperlinkLabelControlShowClaimLinks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hyperlinkLabelControlShowClaimLinks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hyperlinkLabelControlShowClaimLinks.Location = new System.Drawing.Point(170, 15);
            this.hyperlinkLabelControlShowClaimLinks.Name = "hyperlinkLabelControlShowClaimLinks";
            this.hyperlinkLabelControlShowClaimLinks.Size = new System.Drawing.Size(52, 13);
            this.hyperlinkLabelControlShowClaimLinks.TabIndex = 1;
            this.hyperlinkLabelControlShowClaimLinks.Text = "Hide Options";
            this.hyperlinkLabelControlShowClaimLinks.Visible = false;
            this.hyperlinkLabelControlShowClaimLinks.Click += new System.EventHandler(this.hyperlinkLabelControlShowClaimLinks_Click);
            // 
            // panelControl
            // 
            this.panelControl.Controls.Add(this.pictureBoxClaims);
            this.panelControl.Controls.Add(this.labelControlClaims);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl.Location = new System.Drawing.Point(0, 0);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(240, 48);
            this.panelControl.TabIndex = 0;
            // 
            // pictureBoxClaims
            // 
            this.pictureBoxClaims.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxClaims.Image")));
            this.pictureBoxClaims.Location = new System.Drawing.Point(12, 7);
            this.pictureBoxClaims.Name = "pictureBoxClaims";
            this.pictureBoxClaims.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxClaims.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxClaims.TabIndex = 0;
            this.pictureBoxClaims.TabStop = false;
            // 
            // labelControlClaims
            // 
            this.labelControlClaims.Location = new System.Drawing.Point(52, 14);
            this.labelControlClaims.Name = "labelControlClaims";
            this.labelControlClaims.Size = new System.Drawing.Size(57, 14);
            this.labelControlClaims.TabIndex = 0;
            this.labelControlClaims.Text = "Claims\r\n";
            // 
            // ClaimWidgetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControlClaims);
            this.Name = "ClaimWidgetControl";
            this.Size = new System.Drawing.Size(240, 200);
            this.xtraScrollableControlClaims.ResumeLayout(false);
            this.panelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClaims)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        
        private System.Windows.Forms.PictureBox pictureBoxClaims;
		private System.Windows.Forms.ScrollableControl xtraScrollableControlClaims;
		private System.Windows.Forms.Label labelControlClaims;
		private System.Windows.Forms.Panel panelControl;
		private System.Windows.Forms.Label hyperlinkLabelControlShowClaimLinks;
	}
}
