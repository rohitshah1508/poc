/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
namespace HealthEdge.Manager.Controls
{
	public class TaskLink
	{
		public string EntityType { get; set; }

        public string SearchType { get; set; }

		public string Text { get; set; }

		public string Verb { get; set; }

        public System.Drawing.Image Image { get; set; }
    }
}
