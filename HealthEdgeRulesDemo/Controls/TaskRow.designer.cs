/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using HealthEdge.Controls;

namespace HealthEdge.Manager.Controls
{
	partial class TaskRow
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskRow));
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.panelControlTaskLinks = new DevExpress.XtraEditors.PanelControl();
            this.panelControlTop = new DevExpress.XtraEditors.PanelControl();
            this.panelControlLabel = new DevExpress.XtraEditors.PanelControl();
            this.labelControl = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTaskLinks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTop)).BeginInit();
            this.panelControlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlLabel)).BeginInit();
            this.panelControlLabel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxImage.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxImage.Image")));
            this.pictureBoxImage.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(60, 30);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxImage.TabIndex = 0;
            this.pictureBoxImage.TabStop = false;
            // 
            // panelControlTaskLinks
            // 
            this.panelControlTaskLinks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControlTaskLinks.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControlTaskLinks.Location = new System.Drawing.Point(190, 0);
            this.panelControlTaskLinks.Name = "panelControlTaskLinks";
            this.panelControlTaskLinks.Size = new System.Drawing.Size(87, 30);
            this.panelControlTaskLinks.TabIndex = 1;
            // 
            // panelControlTop
            // 
            this.panelControlTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControlTop.Controls.Add(this.panelControlLabel);
            this.panelControlTop.Controls.Add(this.panelControlTaskLinks);
            this.panelControlTop.Controls.Add(this.pictureBoxImage);
            this.panelControlTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlTop.Location = new System.Drawing.Point(0, 0);
            this.panelControlTop.Name = "panelControlTop";
            this.panelControlTop.Size = new System.Drawing.Size(277, 30);
            this.panelControlTop.TabIndex = 0;
            // 
            // panelControlLabel
            // 
            this.panelControlLabel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControlLabel.Controls.Add(this.labelControl);
            this.panelControlLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControlLabel.Location = new System.Drawing.Point(60, 0);
            this.panelControlLabel.Name = "panelControlLabel";
            this.panelControlLabel.Size = new System.Drawing.Size(131, 30);
            this.panelControlLabel.TabIndex = 0;
            // 
            // labelControl
            // 
            this.labelControl.Location = new System.Drawing.Point(7, 9);
            this.labelControl.Name = "labelControl";
            this.labelControl.Size = new System.Drawing.Size(25, 13);
            this.labelControl.TabIndex = 0;
            this.labelControl.Text = "Label";
            // 
            // TaskRow
            // 
            this.Controls.Add(this.panelControlTop);
            this.Name = "TaskRow";
            this.Size = new System.Drawing.Size(277, 30);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTaskLinks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTop)).EndInit();
            this.panelControlTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlLabel)).EndInit();
            this.panelControlLabel.ResumeLayout(false);
            this.panelControlLabel.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBoxImage;
        private DevExpress.XtraEditors.PanelControl panelControlTaskLinks;
        private DevExpress.XtraEditors.PanelControl panelControlTop;
        private DevExpress.XtraEditors.PanelControl panelControlLabel;
        private DevExpress.XtraEditors.LabelControl labelControl;
	}
}
