/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using HealthEdge.Controls;

namespace HealthEdgeRulesDemo.Controls
{
    partial class HomeTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.documentManager = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.widgetView = new DevExpress.XtraBars.Docking2010.Views.Widget.WidgetView(this.components);
            this.columnDefinition1 = new DevExpress.XtraBars.Docking2010.Views.Widget.ColumnDefinition();
            this.columnDefinition2 = new DevExpress.XtraBars.Docking2010.Views.Widget.ColumnDefinition();
            this.columnDefinition3 = new DevExpress.XtraBars.Docking2010.Views.Widget.ColumnDefinition();
            this.documentWorkbasket = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentClaims = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentMembers = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentFinancials = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentProviders = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentOthers = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentFunding = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentUtilization = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentAdministration = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.documentFavorites = new DevExpress.XtraBars.Docking2010.Views.Widget.Document(this.components);
            this.rowDefinition1 = new DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition();
            this.rowDefinition2 = new DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition();
            this.rowDefinition3 = new DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition();
            this.rowDefinition4 = new DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition();
            this.stackGroup1 = new DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup(this.components);
            this.stackGroup2 = new DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup(this.components);
            this.stackGroup3 = new DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.widgetView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnDefinition1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnDefinition2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnDefinition3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentWorkbasket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentClaims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentMembers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFinancials)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentProviders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentOthers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFunding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentUtilization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentAdministration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFavorites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stackGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stackGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stackGroup3)).BeginInit();
            this.SuspendLayout();
            // 
            // documentManager
            // 
            this.documentManager.ContainerControl = this;
            this.documentManager.ShowThumbnailsInTaskBar = DevExpress.Utils.DefaultBoolean.False;
            this.documentManager.View = this.widgetView;
            this.documentManager.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.widgetView});
            // 
            // widgetView
            // 
            this.widgetView.AllowDocumentStateChangeAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.widgetView.AllowResizeAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.widgetView.AllowStartupAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.widgetView.Columns.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.ColumnDefinition[] {
            this.columnDefinition1,
            this.columnDefinition2,
            this.columnDefinition3});
            this.widgetView.DocumentProperties.AllowClose = true;
            this.widgetView.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            this.documentWorkbasket,
            this.documentClaims,
            this.documentMembers,
            this.documentFinancials,
            this.documentProviders,
            this.documentOthers,
            this.documentFunding,
            this.documentUtilization,
            this.documentAdministration,
            this.documentFavorites});
            this.widgetView.DocumentSpacing = 5;
            this.widgetView.FlowLayoutProperties.FlowLayoutItems.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.Document[] {
            this.documentWorkbasket,
            this.documentClaims,
            this.documentMembers,
            this.documentFinancials,
            this.documentProviders,
            this.documentOthers,
            this.documentFunding,
            this.documentUtilization,
            this.documentAdministration});
            this.widgetView.LayoutMode = DevExpress.XtraBars.Docking2010.Views.Widget.LayoutMode.TableLayout;
            this.widgetView.Rows.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition[] {
            this.rowDefinition1,
            this.rowDefinition2,
            this.rowDefinition3,
            this.rowDefinition4});
            this.widgetView.StackGroups.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup[] {
            this.stackGroup1,
            this.stackGroup2,
            this.stackGroup3});
            // 
            // documentWorkbasket
            // 
            this.documentWorkbasket.Caption = "Workbasket Records Assigned to Me";
            this.documentWorkbasket.ControlName = "documentWorkbasket";
            this.documentWorkbasket.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentClaims
            // 
            this.documentClaims.Caption = "";
            this.documentClaims.ControlName = "documentClaims";
            this.documentClaims.RowIndex = 1;
            this.documentClaims.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentMembers
            // 
            this.documentMembers.Caption = "";
            this.documentMembers.ColumnIndex = 2;
            this.documentMembers.ControlName = "documentMembers";
            this.documentMembers.RowSpan = 2;
            this.documentMembers.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentFinancials
            // 
            this.documentFinancials.Caption = "";
            this.documentFinancials.ColumnIndex = 2;
            this.documentFinancials.ControlName = "documentFinancials";
            this.documentFinancials.RowIndex = 2;
            this.documentFinancials.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentProviders
            // 
            this.documentProviders.Caption = "";
            this.documentProviders.ControlName = "documentProviders";
            this.documentProviders.RowIndex = 2;
            this.documentProviders.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentOthers
            // 
            this.documentOthers.Caption = "";
            this.documentOthers.ColumnIndex = 1;
            this.documentOthers.ControlName = "documentOthers";
            this.documentOthers.RowIndex = 2;
            this.documentOthers.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentFunding
            // 
            this.documentFunding.Caption = "";
            this.documentFunding.ColumnIndex = 1;
            this.documentFunding.ControlName = "documentFunding";
            this.documentFunding.RowIndex = 3;
            this.documentFunding.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentUtilization
            // 
            this.documentUtilization.Caption = "";
            this.documentUtilization.ControlName = "documentUtilization";
            this.documentUtilization.RowIndex = 3;
            this.documentUtilization.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentAdministration
            // 
            this.documentAdministration.Caption = "";
            this.documentAdministration.ColumnIndex = 2;
            this.documentAdministration.ControlName = "documentAdministration";
            this.documentAdministration.RowIndex = 3;
            this.documentAdministration.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentFavorites
            // 
            this.documentFavorites.Caption = "";
            this.documentFavorites.ColumnIndex = 1;
            this.documentFavorites.ControlName = "documentFavorites";
            this.documentFavorites.RowSpan = 2;
            this.documentFavorites.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            // 
            // stackGroup1
            // 
            this.stackGroup1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.Document[] {
            this.documentWorkbasket,
            this.documentClaims,
            this.documentProviders,
            this.documentUtilization});
            // 
            // stackGroup2
            // 
            this.stackGroup2.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.Document[] {
            this.documentFavorites,
            this.documentOthers,
            this.documentAdministration});
            // 
            // stackGroup3
            // 
            this.stackGroup3.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.Document[] {
            this.documentMembers,
            this.documentFinancials,
            this.documentFunding});
            // 
            // HomeTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "HomeTab";
            this.Size = new System.Drawing.Size(826, 752);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.widgetView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnDefinition1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnDefinition2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnDefinition3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentWorkbasket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentClaims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentMembers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFinancials)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentProviders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentOthers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFunding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentUtilization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentAdministration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFavorites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowDefinition4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stackGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stackGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stackGroup3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager;
        private DevExpress.XtraBars.Docking2010.Views.Widget.WidgetView widgetView;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentMembers;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentProviders;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentFinancials;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentClaims;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentFunding;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentUtilization;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentOthers;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentAdministration;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentFavorites;
        private DevExpress.XtraBars.Docking2010.Views.Widget.Document documentWorkbasket;
        private DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup stackGroup1;
        private DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup stackGroup2;
        private DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup stackGroup3;
        private DevExpress.XtraBars.Docking2010.Views.Widget.ColumnDefinition columnDefinition1;
        private DevExpress.XtraBars.Docking2010.Views.Widget.ColumnDefinition columnDefinition2;
        private DevExpress.XtraBars.Docking2010.Views.Widget.ColumnDefinition columnDefinition3;
        private DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition rowDefinition1;
        private DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition rowDefinition2;
        private DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition rowDefinition3;
        private DevExpress.XtraBars.Docking2010.Views.Widget.RowDefinition rowDefinition4;
    }
}
