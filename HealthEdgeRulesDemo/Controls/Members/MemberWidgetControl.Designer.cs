/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
namespace HealthEdgeRulesDemo.Controls.Members
{
    partial class MemberWidgetControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemberWidgetControl));
            this.xtraScrollableControlMembers = new System.Windows.Forms.ScrollableControl();
            this.hyperlinkLabelControlShowMemberLinks = new System.Windows.Forms.Label();
            this.panelControl = new System.Windows.Forms.Panel();
            this.pictureBoxMembers = new System.Windows.Forms.PictureBox();
            this.labelControlMembers = new System.Windows.Forms.Label();
            this.xtraScrollableControlMembers.SuspendLayout();
            this.panelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMembers)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControlMembers
            // 
            this.xtraScrollableControlMembers.Controls.Add(this.hyperlinkLabelControlShowMemberLinks);
            this.xtraScrollableControlMembers.Controls.Add(this.panelControl);
            this.xtraScrollableControlMembers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControlMembers.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControlMembers.Name = "xtraScrollableControlMembers";
            this.xtraScrollableControlMembers.Size = new System.Drawing.Size(240, 200);
            this.xtraScrollableControlMembers.TabIndex = 0;
            // 
            // hyperlinkLabelControlShowMemberLinks
            // 
            this.hyperlinkLabelControlShowMemberLinks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hyperlinkLabelControlShowMemberLinks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hyperlinkLabelControlShowMemberLinks.Location = new System.Drawing.Point(170, 15);
            this.hyperlinkLabelControlShowMemberLinks.Name = "hyperlinkLabelControlShowMemberLinks";
            this.hyperlinkLabelControlShowMemberLinks.Size = new System.Drawing.Size(61, 13);
            this.hyperlinkLabelControlShowMemberLinks.TabIndex = 1;
            this.hyperlinkLabelControlShowMemberLinks.Text = "Hide Options";
            this.hyperlinkLabelControlShowMemberLinks.Visible = false;
            this.hyperlinkLabelControlShowMemberLinks.Click += new System.EventHandler(this.hyperlinkLabelControlShowMemberLinks_Click);
            // 
            // panelControl
            // 
            this.panelControl.Controls.Add(this.pictureBoxMembers);
            this.panelControl.Controls.Add(this.labelControlMembers);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl.Location = new System.Drawing.Point(0, 0);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(240, 48);
            this.panelControl.TabIndex = 0;
            // 
            // pictureBoxMembers
            // 
            this.pictureBoxMembers.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxMembers.Image")));
            this.pictureBoxMembers.Location = new System.Drawing.Point(12, 7);
            this.pictureBoxMembers.Name = "pictureBoxMembers";
            this.pictureBoxMembers.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxMembers.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxMembers.TabIndex = 0;
            this.pictureBoxMembers.TabStop = false;
            // 
            // labelControlMembers
            // 
            this.labelControlMembers.Location = new System.Drawing.Point(52, 14);
            this.labelControlMembers.Name = "labelControlMembers";
            this.labelControlMembers.Size = new System.Drawing.Size(67, 14);
            this.labelControlMembers.TabIndex = 0;
            this.labelControlMembers.Text = "Members";
            // 
            // MemberWidgetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.xtraScrollableControlMembers);
            this.Name = "MemberWidgetControl";
            this.Size = new System.Drawing.Size(240, 200);
            this.xtraScrollableControlMembers.ResumeLayout(false);
            this.panelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMembers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ScrollableControl xtraScrollableControlMembers;
        private System.Windows.Forms.PictureBox pictureBoxMembers;
		private System.Windows.Forms.Label labelControlMembers;
        private System.Windows.Forms.Panel panelControl;
		private System.Windows.Forms.Label hyperlinkLabelControlShowMemberLinks;

    }
}
