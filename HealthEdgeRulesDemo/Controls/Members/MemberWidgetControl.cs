/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using HealthEdge.Framework;


namespace HealthEdgeRulesDemo.Controls.Members
{
    public partial class MemberWidgetControl : UserControl, IWidgetControl
    {
        #region Private Fields

        private const int _fullWidgetHeight = 280;
        private const int _initialWidgetHeight = 230;

        private HomeTabDemo _homeTab;
        private bool _showQuickLinks = false;
        private bool _createdLinks = false;
        
        private Font _headerFont;

        #endregion

        #region Constructor/Destructor

        public MemberWidgetControl(HomeTabDemo homeTab)
        {
            InitializeComponent();

            InitImages();

            _homeTab = homeTab;

            WidgetControl = this;

            WidgetControlName = "Member";// HomeTab.MEMBER_CARD_NAME;

            //hyperlinkLabelControlShowMemberLinks.Text = ApplicationUtil.HideWidgetOption;

            _headerFont = new Font(Font.FontFamily, 10F, FontStyle.Regular);
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {                
                WidgetControl = null;

                _headerFont?.Dispose();                

                components?.Dispose();
            }

            base.Dispose(disposing);
        }

        #endregion

        #region Properties

        public string WidgetControlName { get; set; }

        public Control WidgetControl { get; set; }

        [DefaultValue(false)]
        public bool FillWidgetHeight { get; set; }

        public static int FullWidgetHeight
        {
            get { return _fullWidgetHeight; }
        }

        #endregion

        #region Public Methods

        public void SetShowLinksAtStartup()
        {
            _showQuickLinks = true;
        }

        public void UpdateLinks()
        {
            if (!_showQuickLinks)
                return;

            if (!_createdLinks)
                CreateLinks();
            else
                ShowLinks(true);
        }

        public void HideLinks()
        {
            if (!_showQuickLinks)
                return;

            _showQuickLinks = !_showQuickLinks;
            ShowLinks(false);
        }

        
        #endregion

        #region Private Methods

        private void InitImages()
        {
            //if (HealthEdge.Controller.HealthEdgeController.Instance == null)
            //    return;

            //System.Resources.ResourceManager rm = HealthEdge.Controller.HealthEdgeController.Instance.ResourceMgr;
            //if (rm == null)
            //    return;

            //_searchImage = ((Image)(rm.GetObject(Images.SEARCH_GRAY)));
            //_createImage = ((Image)(rm.GetObject(Images.ADD_GRAY)));
            //_runWizardImage = ((Image)(rm.GetObject(Images.WIZARD_LIGHT_GRAY)));
        }

        private void AddCardHeading()
        {
            labelControlMembers.Text = "Member";// HomeTab.MemberCardLabel;                        
            labelControlMembers.Font = _headerFont;
            labelControlMembers.ForeColor = Color.FromArgb(99, 99, 99);
        }

        private void ShowLinks(bool visible)
        {
            
        }

        private void CreateLinks()
        {
            _createdLinks = true;

            AddCardHeading();
            
        }

		private void hyperlinkLabelControlShowMemberLinks_Click(object sender, EventArgs e)
		{
			_showQuickLinks = !_showQuickLinks;

			if (!_showQuickLinks)
			{
                //hyperlinkLabelControlShowMemberLinks.Text = HealthEdge.Utility.ApplicationUtil.ShowWidgetOption;
				ShowLinks(false);

                //if (FillWidgetHeight && _homeTab != null)
                //    _homeTab.SetWidgetHeight(this, _initialWidgetHeight);
			}
			else
			{
                //hyperlinkLabelControlShowMemberLinks.Text = HealthEdge.Utility.ApplicationUtil.HideWidgetOption;
				UpdateLinks();
			}
		}

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool handled = false;

            if ((msg.Msg == 0x100) || (msg.Msg == 0x104))
            {
                switch (keyData)
                {
                    case Keys.PageDown:
                        xtraScrollableControlMembers.VerticalScroll.Value += this.VerticalScroll.LargeChange;
                        handled = true;
                        break;

                    case Keys.PageUp:
                        xtraScrollableControlMembers.VerticalScroll.Value -= this.VerticalScroll.LargeChange;
                        if (xtraScrollableControlMembers.VerticalScroll.Value < VerticalScroll.LargeChange)
                            xtraScrollableControlMembers.VerticalScroll.Value = 0;
                        else
                            xtraScrollableControlMembers.VerticalScroll.Value -= VerticalScroll.LargeChange;
                        handled = true;
                        break;
                }
            }

            if (!handled)
                handled = base.ProcessCmdKey(ref msg, keyData);

            return handled;
        }

        #endregion
    }
}
