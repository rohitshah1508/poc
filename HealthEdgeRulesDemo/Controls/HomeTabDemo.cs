﻿/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.Linq;
using System.Windows.Forms;

using HealthEdgeRulesDemo.Controls.Claims;
using HealthEdgeRulesDemo.Controls.Members;

namespace HealthEdgeRulesDemo.Controls
{
    public partial class HomeTabDemo : UserControl
    {
        #region Private Fields

        private MemberWidgetControl _memberControl;
        private ClaimWidgetControl _claimControl;

        #endregion

        public HomeTabDemo()
        {
            InitializeComponent();

            this.Load += new EventHandler(HomeTabDemo_Load); 
        }

        private void HomeTabDemo_Load(object sender, EventArgs e)
        {
            if (_memberControl == null)
            {
                _memberControl = new MemberWidgetControl(this);
                flowLayoutPanel1.Controls.Add(_memberControl);
            }

            if (_claimControl == null)
            {
                _claimControl = new ClaimWidgetControl(this);
                flowLayoutPanel.Controls.Add(_claimControl);
            }
        }

        public void ShowControl(string[] rolesMapping, string[] loggedInUserRoles)
        {
            //Check user has permission for mapped roles
            if (loggedInUserRoles?.Count() > 0)
            {
                foreach (string role in rolesMapping)
                {
                    string[] widget = role.Split(new char[] { ':' });
                    if (widget[0].Equals("claims", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (!loggedInUserRoles.Contains(widget[1]))
                        {
                            flowLayoutPanel.Controls.Remove(_claimControl);
                            this.Controls.Remove(flowLayoutPanel);
                        }
                    }
                    else if (widget[0].Equals("member", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (!loggedInUserRoles.Contains(widget[1]))
                        {
                            flowLayoutPanel1.Controls.Remove(_memberControl);
                            this.Controls.Remove(flowLayoutPanel1);
                        }
                    }
                }
            }
            else
            {
                flowLayoutPanel.Controls.Remove(_claimControl);
                this.Controls.Remove(flowLayoutPanel);

                flowLayoutPanel1.Controls.Remove(_memberControl);
                this.Controls.Remove(flowLayoutPanel1);
            }

            this.Refresh();            
        }
    }
}
