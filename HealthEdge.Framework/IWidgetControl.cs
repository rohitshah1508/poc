/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
﻿using System;
using System.Windows.Forms;

namespace HealthEdge.Framework
{
    public interface IWidgetControl
    {
        string WidgetControlName { get; set; }

        Control WidgetControl { get; set; }

        void UpdateLinks();

        void HideLinks();

        void SetShowLinksAtStartup();
    }
}