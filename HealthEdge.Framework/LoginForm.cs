﻿/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using HealthEdge.SecurityAndAdministration;
using HealthEdge.Utility;

namespace HealthEdge.Framework
{
    public partial class LoginForm : Form
    {
        #region Constants

        public const int HT_CAPTION = 0x1501;

        #endregion

        #region Dll Imports

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32 SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        #endregion

        private string _server = string.Empty;
        private string _group = string.Empty;
        private ApplicationConfiguration _applicationConfiguration;

        public LoginForm(ApplicationConfiguration applicationConfiguration)
        {
            InitializeComponent();
            _applicationConfiguration = applicationConfiguration;                        
            this.DialogResult = simpleButtonSubmit.DialogResult = DialogResult.None;
        }       

        public string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public JSONPayloadDataResponse JSONPayloadDataResponse { get; set; }
        public string UserName
        {
            get
            {
                return textBoxUserName.Text;
            }
        }
        
        private void simpleButtonSubmit_Click(object sender, EventArgs e)
        {
           Login();
        }

        /// <summary>
        /// Perform login
        /// </summary>
        private DialogResult Login()
        {
            // Case-sensitive username returned by the server
            string serverUserName = string.Empty;

            string username = textBoxUserName.Text.Trim();

            Cursor currentCursor = Cursor.Current;
            DialogResult dialogResult = DialogResult.None;
            try
            {
                if (!string.IsNullOrEmpty(textBoxUserName.Text))
                {
                    textBoxUserName.Text = username;
                }

                if (textBoxUserName.Text != null)
                {
                    textBoxUserName.Text = textBoxUserName.Text.Trim();
                }

                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    
                    string uri = $"{_applicationConfiguration.ForgeRockHost}/{_applicationConfiguration.Realm}/access_token";
                    JSONPayloadDataResponse = SecurityAdminWrapper.Login(textBoxUserName.Text, textBoxPassword.Text, uri, _applicationConfiguration.Client);
                    if (JSONPayloadDataResponse != null)
                    {
                        this.DialogResult = dialogResult = DialogResult.OK;
                        this.Close();
                        return dialogResult;
                    }
                    else
                    {
                        textEditError.Visible = true;
                        this.DialogResult = dialogResult = DialogResult.None;
                        return dialogResult;
                    }
                }
                finally
                {
                    this.Cursor = currentCursor;
                }              
            }
            catch (HttpListenerException ex)
            {
                this.Cursor = currentCursor;
                Console.Error.WriteLine("Login exception message: " + ex.Message);
                if (ex.Message.IndexOf("401") >= 0 || ex.Message.IndexOf("403") >= 0)
                {
                    textEditError.Visible = true;
                    textBoxPassword.Text = string.Empty;
                    textBoxPassword.Focus();
                    simpleButtonSubmit.DialogResult = DialogResult.None;
                    return dialogResult;
                }
                else
                {                    
                    string parsedErrorMessage = ex.Message;

                    MessageBox.Show("Connection Error", parsedErrorMessage);

                    simpleButtonSubmit.DialogResult = DialogResult.None;
                    return dialogResult;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = currentCursor;
                MessageBox.Show(this, ex.ToString());
                return dialogResult;
            }
            finally
            {
                this.Cursor = currentCursor;
            }            
        }

        public bool IsLoginRequired()
        {
            JSONPayloadDataResponse = SecurityAdminWrapper.IsLoginRequired($"{_applicationConfiguration.ForgeRockHost}/{_applicationConfiguration.Realm}/introspect", _applicationConfiguration.Client);

            if (JSONPayloadDataResponse != null)
                return JSONPayloadDataResponse.IsActive ? false : true;

            return true;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            SendMessage(textBoxUserName.Handle, HT_CAPTION, 0, "Username");
            SendMessage(textBoxPassword.Handle, HT_CAPTION, 0, "Password");            

            string path = $"{AssemblyDirectory}{Path.DirectorySeparatorChar}HealthEdge.Manager.resources.dll";
            Assembly asm = Assembly.LoadFrom(path);

            ResourceManager rm = new System.Resources.ResourceManager("HealthEdge.Manager", asm);
            this.pictureBoxTitle.Image = ((System.Drawing.Image)(rm.GetObject("ManagerLoginTitle")));
            
            this.BackgroundImage = ((System.Drawing.Image)(rm.GetObject("LoginBackground")));

            buttonCancel.Focus();
            buttonCancel.Select();
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            simpleButtonSubmit.Enabled = (!string.IsNullOrEmpty(textBoxUserName.Text) && !string.IsNullOrEmpty(textBoxPassword.Text));

            if (textBoxPassword.Text.Length >= 1)
            {
                textEditError.Visible = false;
            }
        }
    }
}
