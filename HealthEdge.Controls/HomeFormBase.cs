/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.Reflection;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace HealthEdge.Controls
{
    public class HomeFormBase : Form
    {
        [DllImport("user32")]
        protected static extern int ShowWindow(int hwnd, int nCmdShow);

        [DllImport("user32.dll")]
        protected static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        protected static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        protected static extern int ReleaseDC(IntPtr hWnd, IntPtr hDc);

        [DllImport("User32.dll")]
        protected static extern IntPtr GetWindowDC(IntPtr hWnd);

        protected const int HEADER_VERTICAL_OFFSET = 5;
        protected const int WM_ACTIVATEAPP = 0x1c;
        protected const int WA_ACTIVE = 1;

        protected const int WM_APPCOMMAND = 0x0319;
        protected const int APPCOMMAND_BROWSER_BACKWARD = 1;
        protected const int APPCOMMAND_BROWSER_FORWARD = 2;
        
        protected Image _errorImage;
        protected bool _set = false;
        protected bool _handleWarningDisplayed = false;

        public System.Resources.ResourceManager rm;

        

		public HomeFormBase()
		{
			
		}

        #region Disposable
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {                

                _errorImage?.Dispose();
                _errorImage = null;
            }

            base.Dispose(disposing);
        }

        ~HomeFormBase()
        {
            Dispose(false);
        }

        #endregion

        #region Properties

        protected virtual string ResName { get; set; }

        public string UserName { get; set; }

        public string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion        

        #region Protected Methods

        protected void LoadAssembly()
        {            
            string path = $"{AssemblyDirectory}{Path.DirectorySeparatorChar}HealthEdge.Manager.resources.dll";

            Assembly asm;

            try
            {
                asm = Assembly.LoadFrom(path);
                rm = new System.Resources.ResourceManager("HealthEdge.Manager", asm);
                return;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
                this.Close();
                Application.Exit();
                return;
            }
        }
        
        #endregion

        #region Private Methods

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // HomeFormBase
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Name = "HomeFormBase";
            this.ResumeLayout(false);
        }        

        #endregion
       
    }
}
