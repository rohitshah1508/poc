﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HealthEdge.SecurityAndAdministration
{
    [Serializable]
    [XmlRoot(ElementName = "JSONPayloadDataResponse")]
    public class JSONPayloadDataResponse
    {
        #region Properties

        public string AccessToken { get; set; }

        public string EndPoint { get; set; }        

        public string[] Roles { get; set; }

        public string UserName { get; set; }

        public bool IsActive { get; set; }

        #endregion
    }
}
