﻿/******************************************************************************
 *
 * Copyright © 2001-2017 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 *
 *****************************************************************************/
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;

namespace HealthEdge.SecurityAndAdministration
{
    public static class SecurityAdminWrapper
    {
        private static HttpClient _httpClient = new HttpClient();
        private static string _cachedFilePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\HealthEdge\POC_Demo.xml";        

        public static JSONPayloadDataResponse Login(string usreName, string password, string uri, string client)
        {            
            try
            {                
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), uri))
                {
                    request.Headers.TryAddWithoutValidation("Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes(client))}");

                    var contentList = new List<string>();
                    contentList.Add($"grant_type={Uri.EscapeDataString("password")}");
                    contentList.Add($"username={Uri.EscapeDataString(usreName)}");
                    contentList.Add($"password={Uri.EscapeDataString(password)}");
                    contentList.Add($"scope={Uri.EscapeDataString("openid profile roles")}");
                    request.Content = new StringContent(string.Join("&", contentList));
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

                    HttpResponseMessage response = _httpClient.SendAsync(request).Result;
                    if (response?.StatusCode == HttpStatusCode.OK)
                    {
                        var jsonString = response.Content.ReadAsStringAsync().Result;
                        JwtPayload payLoad = JwtPayload.Deserialize(jsonString);

                        JSONPayloadDataResponse jSONPayloadDataResponse = new JSONPayloadDataResponse();
                        jSONPayloadDataResponse.AccessToken = payLoad["access_token"].ToString();
                        jSONPayloadDataResponse.UserName = usreName;
                        JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(payLoad["id_token"].ToString());
                        if (jwtSecurityToken != null)
                        {                           
                            string pattern = @"[]" + "[\"" + " " + Environment.NewLine + "]";
                            string roles = jwtSecurityToken.Payload["roles"].ToString();
                            string spiltRolesToArray = Regex.Replace(roles, pattern, string.Empty);
                            if (!string.IsNullOrEmpty(spiltRolesToArray))
                                jSONPayloadDataResponse.Roles = spiltRolesToArray.Split(new char[] { ',' });

                            SerializeJSONResponse(jSONPayloadDataResponse);
                        }
                        return jSONPayloadDataResponse;                                                      
                    }
                    else
                    {                        
                        return null;
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                
            }
        }
        
        public static JSONPayloadDataResponse IsLoginRequired(string uri, string client)
        {            
            if (!File.Exists(_cachedFilePath))
                return null;
            try
            {
                return InvokeRequest(uri, client);                
            }
            catch (Exception)
            {

            }
            return null;
        }

        public static void Logout(string uri, string client)
        {            
            try
            {                
                InvokeRequest(uri, client);
                bool sessionFileExists = File.Exists(_cachedFilePath);
                if (sessionFileExists)
                    File.Delete(_cachedFilePath);
            }
            catch (Exception)
            {

            }
        }

        private static JSONPayloadDataResponse InvokeRequest(string uri, string client)
        {
            using (var request = new HttpRequestMessage(new HttpMethod("POST"), uri))
            {
                request.Headers.TryAddWithoutValidation("Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes(client))}");

                // Get the JSONPayloadDataResponse from serialized file 
                JSONPayloadDataResponse jsonPayloadDataResponse = DeSerialize();
                if (jsonPayloadDataResponse != null)
                {
                    request.Content = new StringContent($"token={Uri.EscapeDataString(jsonPayloadDataResponse.AccessToken)}");
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

                    HttpResponseMessage response = _httpClient.SendAsync(request).Result;

                    if (response?.StatusCode == HttpStatusCode.OK)
                    {
                        var jsonString = response.Content.ReadAsStringAsync().Result;
                        JwtPayload payLoad = JwtPayload.Deserialize(jsonString);                        
                        jsonPayloadDataResponse.IsActive = Convert.ToBoolean(payLoad["active"].ToString());
                        return jsonPayloadDataResponse;
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        private static void SerializeJSONResponse(JSONPayloadDataResponse jSONPayloadDataResponse)
        {
            if (!Directory.Exists(Path.GetDirectoryName(_cachedFilePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_cachedFilePath));
            }

            var writer = new System.Xml.Serialization.XmlSerializer(typeof(JSONPayloadDataResponse));
            var wfile = new StreamWriter(_cachedFilePath);
            writer.Serialize(wfile, jSONPayloadDataResponse);
            wfile.Close();
        }

        private static JSONPayloadDataResponse DeSerialize()
        {
            if (File.Exists(_cachedFilePath))
            {
                System.Xml.Serialization.XmlSerializer reader =
                                        new System.Xml.Serialization.XmlSerializer(typeof(JSONPayloadDataResponse));
                System.IO.StreamReader file = new System.IO.StreamReader(_cachedFilePath);
                JSONPayloadDataResponse overview = (JSONPayloadDataResponse)reader.Deserialize(file);
                file.Close();
                return overview;
            }

            return null;
        }
    }
}