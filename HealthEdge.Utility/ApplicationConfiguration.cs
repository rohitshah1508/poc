﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthEdge.Utility
{
    public class ApplicationConfiguration
    {
        public string ForgeRockHost { get; set; } = "https://healthedge.frdpcloud.com/am/oauth2/realms/root/realms/";
        public string Realm { get; set; } = "CU3-Default";
        public string Client { get; set; } = "myClient:forgerock";
        public string[] RoleMapping { get; set; } = new string[] { "claims:claimRole", "member:memberRole" };

        public bool ShowLoginAlways { get; set; } = false;
    }
}
